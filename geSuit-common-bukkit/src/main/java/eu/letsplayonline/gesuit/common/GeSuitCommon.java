package eu.letsplayonline.gesuit.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.bukkit.plugin.java.JavaPlugin;

import eu.letsplayonline.gesuit.common.interfaces.Factory;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryRequest;

public class GeSuitCommon extends JavaPlugin {

	private static IMessageManager instance = null;

	@Override
	public void onEnable() {

		instance = new MessageManager();
		this.saveDefaultConfig();
		instance.setBrokerEndpoint(this.getConfig().getString("host"), this
				.getConfig().getString("port"));
		instance.setServerName(this.getConfig().getString("server-name"));
		instance.setQueue(this.getConfig().getString("queue"));
		instance.initialize();
		instance.registerReceiver(new ReceiverBase() {
			@Override
			public void onDiscoveryRequest(DiscoveryRequest message) {
				instance.get().send(
						Factory.newDiscoveryHello(getExternalIp(),
								getServer().getPort()));
			}
		});
		instance.get().send(
				Factory.newDiscoveryHello(getExternalIp(), getServer()
						.getPort()));
	}

	private static String getExternalIp() {
		try {
			HttpsURLConnection c = (HttpsURLConnection) new URL(
					"https://api.ipify.org/?format=plain").openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					c.getInputStream()));
			return br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onDisable() {
		instance.shutdown();
	}

	public static IMessageManager getInstance() {
		return instance;
	}
}
