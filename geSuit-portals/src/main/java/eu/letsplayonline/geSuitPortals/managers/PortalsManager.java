package eu.letsplayonline.geSuitPortals.managers;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;

import eu.letsplayonline.geSuitPortals.geSuitPortals;
import eu.letsplayonline.geSuitPortals.objects.Portal;
import eu.letsplayonline.gesuit.common.GeSuitCommon;
import eu.letsplayonline.gesuit.common.interfaces.Factory;
import eu.letsplayonline.gesuit.common.messages.FillType;
import eu.letsplayonline.gesuit.common.messages.PortalType;

public class PortalsManager {

	public static boolean RECEIVED = false;
	public static HashMap<World, ArrayList<Portal>> PORTALS = new HashMap<>();
	public static HashMap<String, Location> pendingTeleports = new HashMap<>();

	public static void deletePortal(String name, String string) {
		GeSuitCommon.getInstance().get()
				.send(Factory.newDeletePortal(null, name, string));
	}

	public static void removePortal(String name) {
		Portal p = getPortal(name);
		if (p != null) {
			PORTALS.get(p.getWorld()).remove(p);
			p.clearPortal();
		}
	}

	public static Portal getPortal(String name) {
		for (ArrayList<Portal> list : PORTALS.values()) {
			for (Portal p : list) {
				if (p.getName().equals(name)) {
					return p;
				}
			}
		}
		return null;
	}

	public static void getPortalsList(String name) {
		GeSuitCommon.getInstance().get()
				.send(Factory.newListPortals(null, name));
	}

	public static void teleportPlayer(Player p, Portal portal) {
		if (p.hasPermission("gesuit.portals.portal." + portal.getName())
				|| p.hasPermission("gesuit.portals.portal.*")) {
			GeSuitCommon
					.getInstance()
					.get()
					.send(Factory.newTeleportPlayerToPortal(null, p.getName(),
							PortalType.valueOf(portal.getType().toUpperCase()),
							portal.getDestination()));
		} else {
			p.sendMessage("No permission to use portal");
		}
	}

	public static void setPortal(CommandSender sender, String name,
			String type, String dest, String fill) {

		Player p = (Player) sender;
		Selection sel = geSuitPortals.WORLDEDIT.getSelection(p);
		if (sel == null || !(sel instanceof CuboidSelection)) {
			sender.sendMessage("No or invalid Selection for Portal");
		} else {

			Location max = sel.getMaximumPoint();
			Location min = sel.getMinimumPoint();
			GeSuitCommon
					.getInstance()
					.get()
					.send(Factory.newSetPortal(
							null,
							sender.getName(),
							new eu.letsplayonline.gesuit.common.messages.Portal(
									name,
									PortalType.valueOf(type.toUpperCase()),
									dest,
									FillType.valueOf(fill.toUpperCase()),
									new eu.letsplayonline.gesuit.common.messages.Location(
											min.getX(), min.getY(), min.getZ(),
											min.getWorld().getName(), min
													.getYaw(), min.getPitch()),
									new eu.letsplayonline.gesuit.common.messages.Location(
											max.getX(), max.getY(), max.getZ(),
											max.getWorld().getName(), max
													.getYaw(), max.getPitch()))));
		}
	}

	public static void addPortal(final String name, final String type, final String dest,
 final String filltype, final Location max,
			final Location min) {
		Bukkit.getScheduler().runTask(geSuitPortals.INSTANCE, new Runnable() {

			@Override
			public void run() {
				try {
					if (max.getWorld() == null) {
						Bukkit.getConsoleSender().sendMessage(
								ChatColor.RED + "World does not exist portal "
										+ name + " will not load :(");
						return;
					}
					Portal portal = new Portal(name, type, dest, filltype, max,
							min);
					ArrayList<Portal> ps = PORTALS.get(max.getWorld());
					if (ps == null) {
						ps = new ArrayList<>();
						PORTALS.put(max.getWorld(), ps);
					}
					ps.add(portal);
					portal.fillPortal();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}

		});

	}

	public static void requestPortals() {
		GeSuitCommon.getInstance().get().send(Factory.newRequestPortals());
	}
}
