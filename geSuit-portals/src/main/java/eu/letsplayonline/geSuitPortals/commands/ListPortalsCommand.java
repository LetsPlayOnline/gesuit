package eu.letsplayonline.geSuitPortals.commands;

import eu.letsplayonline.geSuitPortals.managers.PortalsManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;


public class ListPortalsCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command,
                             String label, String[] args) {

        PortalsManager.getPortalsList(sender.getName());
        return false;
    }

}
