package eu.letsplayonline.geSuitPortals;

import java.util.ArrayList;

import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import eu.letsplayonline.geSuitPortals.commands.DeletePortalCommand;
import eu.letsplayonline.geSuitPortals.commands.ListPortalsCommand;
import eu.letsplayonline.geSuitPortals.commands.SetPortalCommand;
import eu.letsplayonline.geSuitPortals.listeners.PhysicsListener;
import eu.letsplayonline.geSuitPortals.listeners.PlayerLoginListener;
import eu.letsplayonline.geSuitPortals.listeners.PlayerMoveListener;
import eu.letsplayonline.geSuitPortals.listeners.PortalsMessageListener;
import eu.letsplayonline.geSuitPortals.managers.PortalsManager;
import eu.letsplayonline.geSuitPortals.objects.Portal;
import eu.letsplayonline.gesuit.common.GeSuitCommon;

public class geSuitPortals extends JavaPlugin {
	public static geSuitPortals INSTANCE = null;
	public static WorldEditPlugin WORLDEDIT = null;

	@Override
	public void onEnable() {
		INSTANCE = this;
		loadWorldEdit();
		registerListeners();
		registerChannels();
		registerCommands();
		PortalsManager.requestPortals();
	}

	@Override
	public void onDisable() {
		for (ArrayList<Portal> list : PortalsManager.PORTALS.values()) {
			for (Portal p : list) {
				p.clearPortal();
			}
		}
	}

	private void loadWorldEdit() {
		WORLDEDIT = (WorldEditPlugin) getServer().getPluginManager().getPlugin(
				"WorldEdit");
	}

	private void registerCommands() {
		getCommand("setportal").setExecutor(new SetPortalCommand());
		getCommand("delportal").setExecutor(new DeletePortalCommand());
		getCommand("portals").setExecutor(new ListPortalsCommand());
	}

	private void registerChannels() {
		GeSuitCommon.getInstance().registerReceiver(
				new PortalsMessageListener());
	}

	private void registerListeners() {
		getServer().getPluginManager().registerEvents(new PlayerMoveListener(),
				this);
		getServer().getPluginManager().registerEvents(new PhysicsListener(),
				this);
		getServer().getPluginManager().registerEvents(
				new PlayerLoginListener(), this);
	}
}
