package eu.letsplayonline.geSuitPortals.listeners;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import eu.letsplayonline.geSuitPortals.managers.PermissionsManager;
import eu.letsplayonline.geSuitPortals.managers.PortalsManager;

public class PlayerLoginListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void playerConnect(PlayerJoinEvent e) {
		if (PortalsManager.pendingTeleports
				.containsKey(e.getPlayer().getName())) {
			Location l = PortalsManager.pendingTeleports.get(e.getPlayer()
					.getName());
			PortalsManager.pendingTeleports.remove(e.getPlayer().getName());
			e.getPlayer().teleport(l);
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void setPermissionGroup(final PlayerLoginEvent e) {
		if (e.getPlayer().hasPermission("gesuit.*")) {
			PermissionsManager.addAllPermissions(e.getPlayer());
		} else if (e.getPlayer().hasPermission("gesuit.admin")) {
			PermissionsManager.addAdminPermissions(e.getPlayer());
		} else if (e.getPlayer().hasPermission("gesuit.user")) {
			PermissionsManager.addUserPermissions(e.getPlayer());
		}
	}

}
