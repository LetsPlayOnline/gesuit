package eu.letsplayonline.geSuitPortals.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import eu.letsplayonline.geSuitPortals.managers.PortalsManager;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.Portal;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortalPhy;
import eu.letsplayonline.gesuit.common.messages.portals.SendPortals;

public class PortalsMessageListener extends ReceiverBase {

	@Override
	public void onSendPortals(SendPortals message) {
		try {
			for (Portal portal : message.getPortalList()) {
				eu.letsplayonline.gesuit.common.messages.Location min = portal
						.getMinLocation();
				eu.letsplayonline.gesuit.common.messages.Location max = portal
						.getMaxLocation();
				PortalsManager.addPortal(
						portal.getPortalName(),
						portal.getPortalType().toString(),
						portal.getDestinationName(),
						portal.getFillType().toString(),
						new Location(Bukkit.getWorld(max.getWorld()), max
								.getX(), max.getY(), max.getZ(), (float) max
								.getYaw(), (float) max.getPitch()),
						new Location(Bukkit.getWorld(min.getWorld()), min
								.getX(), min.getY(), min.getZ(), (float) min
								.getYaw(), (float) min.getPitch()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDeletePortalPhy(DeletePortalPhy message) {
		PortalsManager.removePortal(message.getPortalName());
	}
}
