package eu.letsplayonline.geSuitTeleports.managers;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.letsplayonline.geSuitTeleports.geSuitTeleports;
import eu.letsplayonline.geSuitTeleports.utils.LocationUtil;
import eu.letsplayonline.gesuit.common.GeSuitCommon;
import eu.letsplayonline.gesuit.common.interfaces.Factory;

public class TeleportsManager {
	public static HashMap<String, Player> pendingTeleports = new HashMap<>();
	public static HashMap<String, Location> pendingTeleportLocations = new HashMap<>();
	public static ArrayList<Player> ignoreTeleport = new ArrayList<>();

	public static void tpAll(final CommandSender sender,
			final String targetPlayer, final String affectedServer) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newTPAll(null,
											sender.getName(), targetPlayer,
											affectedServer));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void tpaRequest(final CommandSender sender,
			final String targetPlayer) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newTPAHereRequest(null,
											sender.getName(), targetPlayer,
											true));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void tpaHereRequest(final CommandSender sender,
			final String targetPlayer) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newTPAHereRequest(null,
											sender.getName(), targetPlayer,
											false));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void tpAccept(final CommandSender sender) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newTPAReply(null, true,
											sender.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void tpDeny(final String sender) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newTPAReply(null, false,
											sender));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void sendDeathBackLocation(final Player p) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							Location l = p.getLocation();
							eu.letsplayonline.gesuit.common.messages.Location target = new eu.letsplayonline.gesuit.common.messages.Location(
									l.getX(), l.getY(), l.getZ(), l.getWorld()
											.getName(), (float) l.getYaw(),
									(float) l.getPitch());
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newPlayersDeathBackLocation(
											null, p.getName(), target));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void sendTeleportBackLocation(final Player p, boolean empty) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							Location l = p.getLocation();
							eu.letsplayonline.gesuit.common.messages.Location target = new eu.letsplayonline.gesuit.common.messages.Location(
									l.getX(), l.getY(), l.getZ(), l.getWorld()
											.getName(), (float) l.getYaw(),
									(float) l.getPitch());
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory
											.newPlayersTeleportBackLocation(
													null, p.getName(), target));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

	}

	public static void sendPlayerBack(final CommandSender sender) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newSendPlayerBack(
											null,
											sender.getName(),
											sender.hasPermission("gesuit.teleports.back.death"),
											sender.hasPermission("gesuit.teleports.back.teleport")));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

	}

	public static void toggleTeleports(final String name) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory
											.newToggleTeleports(null, name));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void teleportPlayerToPlayer(final String player, String target) {
		Player p = Bukkit.getPlayer(player);
		Player t = Bukkit.getPlayer(target);
		if (p != null) {
			p.teleport(t);
		} else {
			pendingTeleports.put(player, t);
			// clear pending teleport if they dont connect
			Bukkit.getScheduler().runTaskLaterAsynchronously(
					geSuitTeleports.instance, new Runnable() {
						@Override
						public void run() {
							if (pendingTeleports.containsKey(player)) {
								pendingTeleports.remove(player);
							}

						}
					}, 100);
		}
	}

	public static void teleportPlayerToLocation(final String player,
			String world, double x, double y, double z, float yaw, float pitch) {
		Location t = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
		Player p = Bukkit.getPlayer(player);
		if (p != null) {
			// Check if Block is safe
			if (LocationUtil.isBlockUnsafe(t.getWorld(), t.getBlockX(),
					t.getBlockY(), t.getBlockZ())) {
				try {
					Location l = LocationUtil.getSafeDestination(p, t);
					p.teleport(l);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				p.teleport(t);
			}
		} else {
			pendingTeleportLocations.put(player, t);
			// clear pending teleport if they dont connect
			Bukkit.getScheduler().runTaskLaterAsynchronously(
					geSuitTeleports.instance, new Runnable() {
						@Override
						public void run() {
							if (pendingTeleportLocations.containsKey(player)) {
								pendingTeleportLocations.remove(player);
							}
						}
					}, 100);
		}
	}

	public static void teleportToPlayer(final CommandSender sender,
			final String player, final String target) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newTeleportToPlayer(
											null,
											sender.getName(),
											player,
											target,
											sender.hasPermission("gesuit.teleports.tp.silent"),
											sender.hasPermission("gesuit.teleports.tp.bypass")));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void teleportToLocation(final String player,
			final String server, final String world, final Double x,
			final Double y, final Double z) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitTeleports.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							eu.letsplayonline.gesuit.common.messages.Location target = new eu.letsplayonline.gesuit.common.messages.Location(
									x, y, z, world, 0, 0);
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newTeleportToLocation(server,
											player, target));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}
}
