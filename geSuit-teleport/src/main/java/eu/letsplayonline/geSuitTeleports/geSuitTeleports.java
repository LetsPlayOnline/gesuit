package eu.letsplayonline.geSuitTeleports;

import eu.letsplayonline.geSuitTeleports.commands.*;
import eu.letsplayonline.geSuitTeleports.listeners.TeleportsListener;
import eu.letsplayonline.geSuitTeleports.listeners.TeleportsMessageListener;
import eu.letsplayonline.gesuit.common.GeSuitCommon;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class geSuitTeleports extends JavaPlugin {
	public static geSuitTeleports instance;

	@Override
	public void onEnable() {
		instance = this;
		registerListeners();
		registerChannels();
		registerCommands();
	}

	private void registerCommands() {
		getCommand("tp").setExecutor(new TPCommand());
		getCommand("tphere").setExecutor(new TPHereCommand());
		getCommand("tpall").setExecutor(new TPAllCommand());
		getCommand("tpa").setExecutor(new TPACommand());
		getCommand("tpahere").setExecutor(new TPAHereCommand());
		getCommand("tpaccept").setExecutor(new TPAcceptCommand());
		getCommand("tpdeny").setExecutor(new TPDenyCommand());
		getCommand("back").setExecutor(new BackCommand());
		getCommand("tptoggle").setExecutor(new ToggleCommand());
	}

	private void registerChannels() {
		GeSuitCommon.getInstance().registerReceiver(
				new TeleportsMessageListener());
	}

	private void registerListeners() {
		getServer().getPluginManager().registerEvents(new TeleportsListener(),
				this);
	}

}
