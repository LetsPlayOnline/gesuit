package eu.letsplayonline.geSuitTeleports.listeners;

import eu.letsplayonline.geSuitTeleports.managers.TeleportsManager;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.Location;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToPlayer;

public class TeleportsMessageListener extends ReceiverBase {

	@Override
	public void onTeleportToPlayer(TeleportToPlayer message) {
		TeleportsManager.teleportPlayerToPlayer(message.getPlayerToTeleport(),
				message.getPlayerToTeleportTo());
	}

	@Override
	public void onTeleportToLocation(TeleportToLocation message) {
		Location l = message.getTarget();
		TeleportsManager.teleportPlayerToLocation(message.getPlayerName(),
				l.getWorld(), l.getX(), l.getY(), l.getZ(), (float) l.getYaw(),
				(float) l.getPitch());
	}
}
