package eu.letsplayonline.geSuiteSpawn;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import eu.letsplayonline.geSuiteSpawn.commands.GlobalSpawnCommand;
import eu.letsplayonline.geSuiteSpawn.commands.ServerSpawnCommand;
import eu.letsplayonline.geSuiteSpawn.commands.SetGlobalSpawnCommand;
import eu.letsplayonline.geSuiteSpawn.commands.SetNewSpawnCommand;
import eu.letsplayonline.geSuiteSpawn.commands.SetServerSpawnCommand;
import eu.letsplayonline.geSuiteSpawn.commands.SetWorldSpawnCommand;
import eu.letsplayonline.geSuiteSpawn.commands.SpawnCommand;
import eu.letsplayonline.geSuiteSpawn.commands.WorldSpawnCommand;
import eu.letsplayonline.geSuiteSpawn.listeners.SpawnListener;
import eu.letsplayonline.geSuiteSpawn.listeners.SpawnMessageListener;
import eu.letsplayonline.gesuit.common.GeSuitCommon;

public class geSuitSpawn extends JavaPlugin {
	public static Plugin INSTANCE = null;

	@Override
	public void onEnable() {
		INSTANCE = this;
		registerListeners();
		registerChannels();
		registerCommands();
	}

	private void registerCommands() {
		getCommand("setnewspawn").setExecutor(new SetNewSpawnCommand());
		getCommand("setworldspawn").setExecutor(new SetWorldSpawnCommand());
		getCommand("setserverspawn").setExecutor(new SetServerSpawnCommand());
		getCommand("setglobalspawn").setExecutor(new SetGlobalSpawnCommand());
		getCommand("spawn").setExecutor(new SpawnCommand());
		getCommand("worldspawn").setExecutor(new WorldSpawnCommand());
		getCommand("serverspawn").setExecutor(new ServerSpawnCommand());
		getCommand("globalspawn").setExecutor(new GlobalSpawnCommand());
	}

	private void registerChannels() {
		GeSuitCommon.getInstance().registerReceiver(new SpawnMessageListener());
	}

	private void registerListeners() {
		getServer().getPluginManager()
				.registerEvents(new SpawnListener(), this);
	}

}
