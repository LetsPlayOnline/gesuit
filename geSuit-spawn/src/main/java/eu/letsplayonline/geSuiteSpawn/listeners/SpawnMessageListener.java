package eu.letsplayonline.geSuiteSpawn.listeners;

import eu.letsplayonline.geSuiteSpawn.managers.SpawnManager;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.Location;
import eu.letsplayonline.gesuit.common.messages.Spawn;
import eu.letsplayonline.gesuit.common.messages.spawns.SendSpawns;

public class SpawnMessageListener extends ReceiverBase {

	@Override
	public void onSendSpawns(SendSpawns message) {
		for (Spawn spawn : message.getSpawns()) {
			Location l = spawn.getSpawnLocation();
			SpawnManager.addSpawn(spawn.getSpawnName(), l.getWorld(), l.getX(),
					l.getY(), l.getZ(), (float) l.getYaw(),
					(float) l.getPitch());
		}
	}
}
