package eu.letsplayonline.geSuiteSpawn.managers;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.letsplayonline.gesuit.common.GeSuitCommon;
import eu.letsplayonline.gesuit.common.interfaces.Factory;
import eu.letsplayonline.gesuit.common.messages.SpawnType;

public class SpawnManager {
	public static boolean HAS_SPAWNS = false;
	public static HashMap<String, Location> SPAWNS = new HashMap<>();

	public static void sendPlayerToProxySpawn(CommandSender sender,
			boolean silent) {
		GeSuitCommon
				.getInstance()
				.get()
				.send(Factory.newSendToProxySpawn(null, sender.getName(),
						silent));
	}

	public static void setNewPlayerSpawn(CommandSender sender) {
		setNewSpawn(sender, SpawnType.NEW_PLAYER);

	}

	private static void setNewSpawn(CommandSender sender, SpawnType type,
			boolean exists) {
		Player p = (Player) sender;
		Location l = p.getLocation();
		eu.letsplayonline.gesuit.common.messages.Location spawnLocation = new eu.letsplayonline.gesuit.common.messages.Location(
				l.getX(), l.getY(), l.getZ(), l.getWorld().getName(),
				(float) l.getYaw(), (float) l.getPitch());
		GeSuitCommon
				.getInstance()
				.get()
				.send(Factory.newSetSpawn(null, sender.getName(),
						spawnLocation, exists, type));
	}

	private static void setNewSpawn(CommandSender sender, SpawnType type) {
		setNewSpawn(sender, type, false);
	}

	public static void setProxySpawn(CommandSender sender) {
		setNewSpawn(sender, SpawnType.PROXY);
	}

	public static void setWorldSpawn(CommandSender sender) {
		Player p = (Player) sender;
		Location l = p.getLocation();
		p.getWorld().setSpawnLocation(l.getBlockX(), l.getBlockY(),
				l.getBlockZ());
		setNewSpawn(sender, SpawnType.WORLD, hasWorldSpawn(p.getWorld()));
	}

	public static void sendPlayerToServerSpawn(CommandSender sender) {
		Player p = (Player) sender;
		p.teleport(getServerSpawn());
	}

	public static void sendPlayerToWorldSpawn(CommandSender sender) {
		Player p = (Player) sender;
		Location l = getWorldSpawn(p.getWorld());
		if (l == null) {
			p.teleport(p.getWorld().getSpawnLocation());
		} else {
			p.teleport(getWorldSpawn(p.getWorld()));
		}
	}

	public static void getSpawns() {
		GeSuitCommon.getInstance().get().send(Factory.newGetSpawns());
	}

	public static boolean hasWorldSpawn(World w) {
		return SPAWNS.containsKey(w.getName());
	}

	public static Location getWorldSpawn(World w) {
		return SPAWNS.get(w.getName());
	}

	public static boolean hasServerSpawn() {
		return SPAWNS.containsKey("server");
	}

	public static Location getServerSpawn() {
		return SPAWNS.get("server");
	}

	public static void setServerSpawn(CommandSender sender) {
		Player p = (Player) sender;
		Location l = p.getLocation();
		p.getWorld().setSpawnLocation(l.getBlockX(), l.getBlockY(),
				l.getBlockZ());
		setNewSpawn(sender, SpawnType.SERVER, hasServerSpawn());

	}

	public static void sendPlayerToSpawn(CommandSender sender) {
		Player p = (Player) sender;
		if (SpawnManager.hasWorldSpawn(p.getWorld())
				&& p.hasPermission("gesuit.spawns.spawn.world")) {
			p.teleport(getWorldSpawn(p.getWorld()));
		} else if (SpawnManager.hasServerSpawn()
				&& p.hasPermission("gesuit.spawns.spawn.server")) {
			p.teleport(getServerSpawn());
		} else if (p.hasPermission("gesuit.spawns.spawn.global")) {
			SpawnManager.sendPlayerToProxySpawn(p, false);
		}
	}

	public static void addSpawn(String name, String world, double x, double y,
			double z, float yaw, float pitch) {
		SPAWNS.put(name, new Location(Bukkit.getWorld(world), x, y, z, yaw,
				pitch));

	}
}
