package eu.letsplayonline.geSuitHomes.managers;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import eu.letsplayonline.geSuitHomes.geSuitHomes;
import eu.letsplayonline.gesuit.common.GeSuitCommon;
import eu.letsplayonline.gesuit.common.interfaces.Factory;

public class HomesManager {

	public static void deleteHome(final CommandSender sender, final String home) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitHomes.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newDeleteHome(null,
											sender.getName(), home));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void sendHome(final CommandSender sender, final String home) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitHomes.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newSendPlayerHome(null,
											sender.getName(), home));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

	}

	public static void getHomesList(final CommandSender sender) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitHomes.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newGetHomesList(null,
											sender.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void importHomes(CommandSender sender) {
		String path = "plugins/Essentials/userdata";
		File folder = new File(path);

		File[] listOfFiles = folder.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".yml");
			}
		});

		int userCount = 0;
		int userHomeCount = 0;
		int homeCount = 0;
		for (File data : listOfFiles) {
			userCount++;
			FileConfiguration user = YamlConfiguration.loadConfiguration(data);
			if (user.contains("homes")) {
				userHomeCount++;
				Set<String> homedata = user.getConfigurationSection("homes")
						.getKeys(false);
				if (homedata != null) {
					for (String homes : homedata) {
						Location loc = new Location(Bukkit.getWorld(user
								.getString("homes." + homes + ".world")),
								user.getDouble("homes." + homes + ".x"),
								user.getDouble("homes." + homes + ".y"),
								user.getDouble("homes." + homes + ".z"),
								(float) user.getDouble("homes." + homes
										+ ".yaw"),
								(float) user.getDouble("homes." + homes
										+ ".pitch"));
						setHome(data.getName().substring(0,
								data.getName().length() - 4), homes, loc);
						homeCount++;
					}
				}
			}
		}
		sender.sendMessage(ChatColor.GOLD + "Out of " + userCount + "users, "
				+ userHomeCount + " had homes");
		sender.sendMessage(ChatColor.GOLD + "Homes imported: " + homeCount);

	}

	public static void reloadHomes(CommandSender sender) {

	}

	public static void setHome(final CommandSender sender, final String home) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitHomes.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							Location l = ((Player) sender).getLocation();
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newSetPlayerHome(
											null,
											sender.getName(),
											getServerHomesLimit(sender),
											getGlobalHomesLimit(sender),
											home,
											new eu.letsplayonline.gesuit.common.messages.Location(
													l.getX(), l.getY(), l
															.getZ(), l
															.getWorld()
															.getName(), l
															.getYaw(), l
															.getPitch())));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void setHome(final String sender, final String home,
			final Location l) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitHomes.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory
											.newSetPlayerHome(
													null,
													sender,
													100,
													300,
													home,
													new eu.letsplayonline.gesuit.common.messages.Location(
															l.getX(), l.getY(),
															l.getZ(), l
																	.getWorld()
																	.getName(),
															l.getYaw(), l
																	.getPitch())));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static int getServerHomesLimit(CommandSender p) {
		int max = 0;

		int maxlimit = 100;

		if (p.hasPermission("gesuit.homes.limits.server.*")
				|| p.hasPermission("gesuit.homes.limits.*")) {
			return maxlimit;
		} else {
			for (int ctr = 0; ctr < maxlimit; ctr++) {
				if (p.hasPermission("gesuit.homes.limits.server." + ctr)) {
					max = ctr;
				}
			}

		}

		return max;
	}

	public static int getGlobalHomesLimit(CommandSender p) {
		int max = 0;

		int maxlimit = 300;

		if (p.hasPermission("gesuit.homes.limits.global.*")
				|| p.hasPermission("gesuit.homes.limits.*")) {
			return maxlimit;
		} else {
			for (int ctr = 0; ctr < maxlimit; ctr++) {
				if (p.hasPermission("gesuit.homes.limits.global." + ctr)) {
					max = ctr;
				}
			}

		}
		return max;
	}
}
