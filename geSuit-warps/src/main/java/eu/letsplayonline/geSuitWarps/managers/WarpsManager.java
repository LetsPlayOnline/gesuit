package eu.letsplayonline.geSuitWarps.managers;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.letsplayonline.geSuitWarps.geSuitWarps;
import eu.letsplayonline.gesuit.common.GeSuitCommon;
import eu.letsplayonline.gesuit.common.interfaces.Factory;

public class WarpsManager {
	public static void warpPlayer(final CommandSender sender,
			final String player, final String warp) {
		if (sender.hasPermission("gesuit.warps.warp." + warp.toLowerCase())
				|| sender.hasPermission("gesuit.warps.warp.*")) {
			Bukkit.getScheduler().runTaskAsynchronously(geSuitWarps.instance,
					new Runnable() {

						@Override
						public void run() {
							try {
								GeSuitCommon
										.getInstance()
										.get()
										.send(Factory.newWarpPlayer(
												null,
												player,
												warp,
												sender.hasPermission("gesuit.warps.bypass")));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
		} else {
			sender.sendMessage("No permission for that");
		}
	}

	public static void setWarp(final CommandSender sender, final String name,
			final boolean hidden, final boolean global) {
		Location l = ((Player) sender).getLocation();
		final eu.letsplayonline.gesuit.common.messages.Location warpLocation = new eu.letsplayonline.gesuit.common.messages.Location(
				l.getX(), l.getY(), l.getZ(), l.getWorld().getName(),
				l.getYaw(), l.getPitch());
		Bukkit.getScheduler().runTaskAsynchronously(geSuitWarps.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newSetWarp(null,
											sender.getName(), name,
											warpLocation, global, hidden));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void deleteWarp(final CommandSender sender, final String warp) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitWarps.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newDeleteWarp(null,
											sender.getName(), warp));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	public static void listWarps(final CommandSender sender) {
		Bukkit.getScheduler().runTaskAsynchronously(geSuitWarps.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newGetWarpsList(
											null,
											sender.getName(),
											sender.hasPermission("gesuit.warps.list.server"),
											sender.hasPermission("gesuit.warps.list.global"),
											sender.hasPermission("gesuit.warps.list.hidden"),
											sender.hasPermission("gesuit.warps.bypass")));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}
}