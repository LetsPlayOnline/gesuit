package eu.letsplayonline.geSuitWarps;

import org.bukkit.plugin.java.JavaPlugin;

import eu.letsplayonline.geSuitWarps.commands.DeleteWarpCommand;
import eu.letsplayonline.geSuitWarps.commands.ListWarpsCommand;
import eu.letsplayonline.geSuitWarps.commands.SetWarpCommand;
import eu.letsplayonline.geSuitWarps.commands.WarpCommand;
import eu.letsplayonline.geSuitWarps.listeners.WarpsListener;

public class geSuitWarps extends JavaPlugin {
	public static geSuitWarps instance;

	@Override
	public void onEnable() {
		instance = this;
		registerListeners();
		registerCommands();
	}

	private void registerCommands() {
		getCommand("warp").setExecutor(new WarpCommand());
		getCommand("warps").setExecutor(new ListWarpsCommand());
		getCommand("setwarp").setExecutor(new SetWarpCommand());
		getCommand("delwarp").setExecutor(new DeleteWarpCommand());
	}

	private void registerListeners() {
		getServer().getPluginManager()
				.registerEvents(new WarpsListener(), this);
	}

}
