package eu.letsplayonline.gesuit.common.messages.portals;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class RequestPortals extends Message {
	public RequestPortals(String targetServer, String name, String serial) {
		super(targetServer, name, serial);
	}

	@Override
	public String toString() {
		return "RequestPortals: " + getTargetServer() + " - " + getSender()
				+ " - " + getSerial();
	}
}
