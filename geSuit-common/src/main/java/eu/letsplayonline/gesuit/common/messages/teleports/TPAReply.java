package eu.letsplayonline.gesuit.common.messages.teleports;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class TPAReply extends Message {
	private Boolean accept;
	private String playerName;

	public TPAReply(String targetServer, String sender, String serial,
			Boolean accept, String playerName) {
		super(targetServer, sender, serial);
		this.accept = accept;
		this.playerName = playerName;
	}

	public TPAReply() {
	}

	public boolean isAccept() {
		return this.accept;
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public void setAccept(boolean accept) {
		this.accept = accept;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof TPAReply))
			return false;
		final TPAReply other = (TPAReply) o;
		if (!other.canEqual((Object) this))
			return false;
		if (this.accept != other.accept)
			return false;
		final Object this$playerName = this.playerName;
		final Object other$playerName = other.playerName;
		if (this$playerName == null ? other$playerName != null
				: !this$playerName.equals(other$playerName))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		result = result * PRIME + (this.accept ? 79 : 97);
		final Object $playerName = this.playerName;
		result = result * PRIME
				+ ($playerName == null ? 0 : $playerName.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof TPAReply;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.teleports.TPAReply(accept="
				+ this.accept + ", playerName=" + this.playerName + ")";
	}
}
