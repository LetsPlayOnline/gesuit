package eu.letsplayonline.gesuit.common.messages;

import java.util.Objects;

/**
 * Serializable Location object containing all info a normal Minecraft Location
 * also contains.
 *
 * @author Julian Fölsch, jfrank
 */
public class Location {
	/** The world-global x position. */
	private double x;
	/** The world-global y position. */
	private double y;
	/** The world-global z position. */
	private double z;
	/** The name of the world. */
	private String world;
	/** The player's yaw. */
	private double yaw;
	/** The player's pitch. */
	private double pitch;

	/**
	 * Constructor which sets all fields.
	 * 
	 * @param x
	 *            x location.
	 * @param y
	 *            y location.
	 * @param z
	 *            z location.
	 * @param world
	 *            world name.
	 * @param yaw
	 *            player yaw.
	 * @param pitch
	 *            player pitch.
	 */
	public Location(double x, double y, double z, String world, double yaw,
			double pitch) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.world = world;
		this.yaw = yaw;
		this.pitch = pitch;
	}

	/**
	 * Default constructor for bean deserialization.
	 */
	public Location() {
	}

	/**
	 * Getter for the x coordinate.
	 * 
	 * @return the x coordinate.
	 */
	public double getX() {
		return x;
	}

	/**
	 * Setter for the x coordinate.
	 * 
	 * @param x
	 *            the x coordinate.
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Getter for the y coordinate.
	 * 
	 * @return the y coordinate.
	 */
	public double getY() {
		return y;
	}

	/**
	 * Setter for the y coordinate.
	 * 
	 * @param y
	 *            the y coordinate.
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * Getter for the z coordinate.
	 * 
	 * @return the z coordinate.
	 */
	public double getZ() {
		return z;
	}

	/**
	 * Setter for the z coordinate.
	 * 
	 * @param z
	 *            the z coordinate.
	 */
	public void setZ(double z) {
		this.z = z;
	}

	/**
	 * Getter for the world name.
	 * 
	 * @return the world name.
	 */
	public String getWorld() {
		return world;
	}

	/**
	 * Setter for the world name.
	 * 
	 * @param world
	 *            the world name.
	 */
	public void setWorld(String world) {
		this.world = world;
	}

	/**
	 * Getter for the yaw value.
	 * 
	 * @return the yaw value.
	 */
	public double getYaw() {
		return yaw;
	}

	/**
	 * Setter for the yaw value.
	 * 
	 * @param yaw
	 *            the yaw value.
	 */
	public void setYaw(double yaw) {
		this.yaw = yaw;
	}

	/**
	 * Getter for the pitch value.
	 * 
	 * @return the pitch value.
	 */
	public double getPitch() {
		return pitch;
	}

	/**
	 * Setter for the pitch value.
	 * 
	 * @param pitch
	 *            the pitch value.
	 */
	public void setPitch(double pitch) {
		this.pitch = pitch;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Location location = (Location) o;
		return Objects.equals(x, location.x) && Objects.equals(y, location.y)
				&& Objects.equals(z, location.z)
				&& Objects.equals(yaw, location.yaw)
				&& Objects.equals(pitch, location.pitch)
				&& Objects.equals(world, location.world);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y, z, world, yaw, pitch);
	}

	@Override
	public String toString() {
		return "Location{" + "x=" + x + ", y=" + y + ", z=" + z + ", world='"
				+ world + '\'' + ", yaw=" + yaw + ", pitch=" + pitch + '}';
	}
}
