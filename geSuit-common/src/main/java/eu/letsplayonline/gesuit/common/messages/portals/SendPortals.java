package eu.letsplayonline.gesuit.common.messages.portals;

import java.util.List;

import eu.letsplayonline.gesuit.common.messages.Message;
import eu.letsplayonline.gesuit.common.messages.Portal;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class SendPortals extends Message {
	List<Portal> portalList;

	public SendPortals(String targetServer, String sender, String serial,
			List<Portal> portalList) {
		super(targetServer, sender, serial);
		this.portalList = portalList;
	}

	public SendPortals() {
	}

	public List<Portal> getPortalList() {
		return this.portalList;
	}

	public void setPortalList(List<Portal> portalList) {
		this.portalList = portalList;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof SendPortals))
			return false;
		final SendPortals other = (SendPortals) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$portalList = this.portalList;
		final Object other$portalList = other.portalList;
		if (this$portalList == null ? other$portalList != null
				: !this$portalList.equals(other$portalList))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $portalList = this.portalList;
		result = result * PRIME
				+ ($portalList == null ? 0 : $portalList.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof SendPortals;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.portals.SendPortal(portalList="
				+ this.portalList + ")";
	}
}
