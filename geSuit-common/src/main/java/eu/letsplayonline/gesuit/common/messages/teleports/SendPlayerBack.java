package eu.letsplayonline.gesuit.common.messages.teleports;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class SendPlayerBack extends Message {
	private String playerName;
	private boolean death;
	private boolean teleport;

	public SendPlayerBack(String targetServer, String sender, String serial,
			String playerName, boolean death, boolean teleport) {
		super(targetServer, sender, serial);
		this.playerName = playerName;
		this.death = death;
		this.teleport = teleport;
	}

	public SendPlayerBack() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public boolean isDeath() {
		return this.death;
	}

	public boolean isTeleport() {
		return this.teleport;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setDeath(boolean death) {
		this.death = death;
	}

	public void setTeleport(boolean teleport) {
		this.teleport = teleport;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof SendPlayerBack))
			return false;
		final SendPlayerBack other = (SendPlayerBack) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$playerName = this.playerName;
		final Object other$playerName = other.playerName;
		if (this$playerName == null ? other$playerName != null
				: !this$playerName.equals(other$playerName))
			return false;
		if (this.death != other.death)
			return false;
		if (this.teleport != other.teleport)
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $playerName = this.playerName;
		result = result * PRIME
				+ ($playerName == null ? 0 : $playerName.hashCode());
		result = result * PRIME + (this.death ? 79 : 97);
		result = result * PRIME + (this.teleport ? 79 : 97);
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof SendPlayerBack;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.teleports.SendPlayerBack(playerName="
				+ this.playerName
				+ ", death="
				+ this.death
				+ ", teleport="
				+ this.teleport + ")";
	}
}
