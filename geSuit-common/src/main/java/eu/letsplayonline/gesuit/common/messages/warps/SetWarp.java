package eu.letsplayonline.gesuit.common.messages.warps;

import eu.letsplayonline.gesuit.common.messages.Location;
import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class SetWarp extends Message {
	private String playerName;
	private String warpName;
	private Location warpLocation;
	private Boolean global;
	private Boolean hidden;

	public SetWarp(String targetServer, String sender, String serial,
			String PlayerName, String WarpName, Location WarpLocation,
			Boolean Global, Boolean Hidden) {
		super(targetServer, sender, serial);
		this.playerName = PlayerName;
		this.warpName = WarpName;
		this.warpLocation = WarpLocation;
		this.global = Global;
		this.hidden = Hidden;
	}

	public SetWarp() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public String getWarpName() {
		return this.warpName;
	}

	public Location getWarpLocation() {
		return this.warpLocation;
	}

	public Boolean isGlobal() {
		return this.global;
	}

	public Boolean isHidden() {
		return this.hidden;
	}

	public void setPlayerName(String PlayerName) {
		this.playerName = PlayerName;
	}

	public void setWarpName(String WarpName) {
		this.warpName = WarpName;
	}

	public void setWarpLocation(Location WarpLocation) {
		this.warpLocation = WarpLocation;
	}

	public void setGlobal(Boolean Global) {
		this.global = Global;
	}

	public void setHidden(Boolean Hidden) {
		this.hidden = Hidden;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof SetWarp))
			return false;
		final SetWarp other = (SetWarp) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$PlayerName = this.playerName;
		final Object other$PlayerName = other.playerName;
		if (this$PlayerName == null ? other$PlayerName != null
				: !this$PlayerName.equals(other$PlayerName))
			return false;
		final Object this$WarpName = this.warpName;
		final Object other$WarpName = other.warpName;
		if (this$WarpName == null ? other$WarpName != null : !this$WarpName
				.equals(other$WarpName))
			return false;
		final Object this$WarpLocation = this.warpLocation;
		final Object other$WarpLocation = other.warpLocation;
		if (this$WarpLocation == null ? other$WarpLocation != null
				: !this$WarpLocation.equals(other$WarpLocation))
			return false;
		final Object this$Global = this.global;
		final Object other$Global = other.global;
		if (this$Global == null ? other$Global != null : !this$Global
				.equals(other$Global))
			return false;
		final Object this$Hidden = this.hidden;
		final Object other$Hidden = other.hidden;
		if (this$Hidden == null ? other$Hidden != null : !this$Hidden
				.equals(other$Hidden))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $PlayerName = this.playerName;
		result = result * PRIME
				+ ($PlayerName == null ? 0 : $PlayerName.hashCode());
		final Object $WarpName = this.warpName;
		result = result * PRIME
				+ ($WarpName == null ? 0 : $WarpName.hashCode());
		final Object $WarpLocation = this.warpLocation;
		result = result * PRIME
				+ ($WarpLocation == null ? 0 : $WarpLocation.hashCode());
		final Object $Global = this.global;
		result = result * PRIME + ($Global == null ? 0 : $Global.hashCode());
		final Object $Hidden = this.hidden;
		result = result * PRIME + ($Hidden == null ? 0 : $Hidden.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof SetWarp;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.warps.SetWarp(playerName="
				+ this.playerName
				+ ", warpName="
				+ this.warpName
				+ ", warpLocation="
				+ this.warpLocation
				+ ", global="
				+ this.global + ", hidden=" + this.hidden + ")";
	}
}
