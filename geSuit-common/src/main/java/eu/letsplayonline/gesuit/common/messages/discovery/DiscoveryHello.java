package eu.letsplayonline.gesuit.common.messages.discovery;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * This message is sent by the leafs when they come up, to signal any listening
 * server that they are there.
 * 
 * @author jfrank
 *
 */
public class DiscoveryHello extends Message {
	/** The listening port of the server. */
	private int port;
	/** The hostname of the server. */
	private String host;

	public DiscoveryHello(String targetServer, String name, String serial,
			String host, int port) {
		super(targetServer, name, serial);
		this.port = port;
		this.host = host;
	}

	@Override
	public String toString() {
		return "DiscoveryHello: " + getTargetServer() + " - " + getSender()
				+ " - " + getSerial() + " - " + host + ":" + port;
	}

	/**
	 * Getter for the port.
	 * 
	 * @return the port.
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Setter for the port.
	 * 
	 * @param port
	 *            the port.
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * Getter for the host.
	 * 
	 * @return the host.
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Setter for the host.
	 * 
	 * @param host
	 *            the host.
	 */
	public void setHost(String host) {
		this.host = host;
	}
}
