package eu.letsplayonline.gesuit.common.messages.portals;

import java.util.Objects;

import eu.letsplayonline.gesuit.common.messages.Message;
import eu.letsplayonline.gesuit.common.messages.PortalType;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class TeleportPlayerToPortal extends Message {
	private String playerName;
	private PortalType portalType;
	private String destinationName;

	public TeleportPlayerToPortal(String targetServer, String name,
			String serial, String playerName, PortalType portalType,
			String destinationName) {
		super(targetServer, name, serial);
		this.playerName = playerName;
		this.portalType = portalType;
		this.destinationName = destinationName;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public PortalType getPortalType() {
		return portalType;
	}

	public void setPortalType(PortalType portalType) {
		this.portalType = portalType;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;
		TeleportPlayerToPortal that = (TeleportPlayerToPortal) o;
		return Objects.equals(playerName, that.playerName)
				&& Objects.equals(portalType, that.portalType)
				&& Objects.equals(destinationName, that.destinationName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), playerName, portalType,
				destinationName);
	}

	@Override
	public String toString() {
		return "TeleportPlayerToPortal{" + "playerName='" + playerName + '\''
				+ ", portalType=" + portalType + ", destinationName='"
				+ destinationName + '\'' + '}';
	}
}
