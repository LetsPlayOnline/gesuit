package eu.letsplayonline.gesuit.common.messages.warps;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class WarpPlayer extends Message {
	private String playerName;
	private String warpName;
	private boolean bypass;

	public WarpPlayer(String targetServer, String sender, String serial,
			String playerName, String warpName, boolean bypass) {
		super(targetServer, sender, serial);
		this.playerName = playerName;
		this.warpName = warpName;
		this.bypass = bypass;
	}

	public WarpPlayer() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public String getWarpName() {
		return this.warpName;
	}

	public boolean isBypass() {
		return this.bypass;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setWarpName(String warpName) {
		this.warpName = warpName;
	}

	public void setBypass(boolean bypass) {
		this.bypass = bypass;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof WarpPlayer))
			return false;
		final WarpPlayer other = (WarpPlayer) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$playerName = this.playerName;
		final Object other$playerName = other.playerName;
		if (this$playerName == null ? other$playerName != null
				: !this$playerName.equals(other$playerName))
			return false;
		final Object this$warpName = this.warpName;
		final Object other$warpName = other.warpName;
		if (this$warpName == null ? other$warpName != null : !this$warpName
				.equals(other$warpName))
			return false;
		if (this.bypass != other.bypass)
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $playerName = this.playerName;
		result = result * PRIME
				+ ($playerName == null ? 0 : $playerName.hashCode());
		final Object $warpName = this.warpName;
		result = result * PRIME
				+ ($warpName == null ? 0 : $warpName.hashCode());
		result = result * PRIME + (this.bypass ? 79 : 97);
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof WarpPlayer;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.warps.WarpPlayer(playerName="
				+ this.playerName
				+ ", warpName="
				+ this.warpName
				+ ", bypass="
				+ this.bypass + ")";
	}
}
