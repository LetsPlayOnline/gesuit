package eu.letsplayonline.gesuit.common.messages.discovery;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * This message is sent by the bungee server to tell every leaf that it should
 * send its coordinates in response.
 * 
 * @author jfrank
 *
 */
public class DiscoveryRequest extends Message {
	public DiscoveryRequest(String targetServer, String name, String serial) {
		super(targetServer, name, serial);
	}

	@Override
	public String toString() {
		return "DiscoveryRequest: " + getTargetServer() + " - " + getSender()
				+ " - " + getSerial();
	}
}
