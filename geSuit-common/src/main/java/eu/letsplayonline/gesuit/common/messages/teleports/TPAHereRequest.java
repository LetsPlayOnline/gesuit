package eu.letsplayonline.gesuit.common.messages.teleports;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class TPAHereRequest extends Message {
	private String playerName;
	private String targetName;
	private boolean meToHim;

	public TPAHereRequest(String targetServer, String sender, String serial,
			String playerName, String targetName, boolean meToHim) {
		super(targetServer, sender, serial);
		this.playerName = playerName;
		this.targetName = targetName;
		this.meToHim = meToHim;
	}

	public TPAHereRequest() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public String getTargetName() {
		return this.targetName;
	}

	public boolean isMeToHim() {
		return this.meToHim;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public void setMeToHim(boolean meToHim) {
		this.meToHim = meToHim;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof TPAHereRequest))
			return false;
		final TPAHereRequest other = (TPAHereRequest) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$playerName = this.playerName;
		final Object other$playerName = other.playerName;
		if (this$playerName == null ? other$playerName != null
				: !this$playerName.equals(other$playerName))
			return false;
		final Object this$targetName = this.targetName;
		final Object other$targetName = other.targetName;
		if (this$targetName == null ? other$targetName != null
				: !this$targetName.equals(other$targetName))
			return false;
		if (this.meToHim != other.meToHim)
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $playerName = this.playerName;
		result = result * PRIME
				+ ($playerName == null ? 0 : $playerName.hashCode());
		final Object $targetName = this.targetName;
		result = result * PRIME
				+ ($targetName == null ? 0 : $targetName.hashCode());
		result = result * PRIME + (this.meToHim ? 79 : 97);
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof TPAHereRequest;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.teleports.TPAHereRequest(playerName="
				+ this.playerName
				+ ", targetName="
				+ this.targetName
				+ ", meToHim=" + this.meToHim + ")";
	}
}
