package eu.letsplayonline.gesuit.common.interfaces;

import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryHello;
import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryRequest;
import eu.letsplayonline.gesuit.common.messages.homes.DeleteHome;
import eu.letsplayonline.gesuit.common.messages.homes.GetHomesList;
import eu.letsplayonline.gesuit.common.messages.homes.SendPlayerHome;
import eu.letsplayonline.gesuit.common.messages.homes.SetPlayerHome;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortal;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortalPhy;
import eu.letsplayonline.gesuit.common.messages.portals.ListPortals;
import eu.letsplayonline.gesuit.common.messages.portals.RequestPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SendPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SetPortal;
import eu.letsplayonline.gesuit.common.messages.portals.TeleportPlayerToPortal;
import eu.letsplayonline.gesuit.common.messages.spawns.GetSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendToProxySpawn;
import eu.letsplayonline.gesuit.common.messages.spawns.SetSpawn;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersDeathBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersTeleportBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.SendPlayerBack;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAHereRequest;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAReply;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAll;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToPlayer;
import eu.letsplayonline.gesuit.common.messages.teleports.ToggleTeleports;
import eu.letsplayonline.gesuit.common.messages.warps.DeleteWarp;
import eu.letsplayonline.gesuit.common.messages.warps.GetWarpsList;
import eu.letsplayonline.gesuit.common.messages.warps.SetWarp;
import eu.letsplayonline.gesuit.common.messages.warps.WarpPlayer;

/**
 * Interface that is called on the appropriate methods whenever a message is
 * received.
 * 
 * @author jfrank
 *
 */
public interface Receiver {

	// Homes
	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onDeleteHome(DeleteHome message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onGetHomesList(GetHomesList message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onSendPlayerHome(SendPlayerHome message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onSetPlayerHome(SetPlayerHome message);

	// Portals
	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onDeletePortal(DeletePortal message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onDeletePortalPhy(DeletePortalPhy message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onListPortals(ListPortals message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onRequestPortals(RequestPortals message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onSendPortals(SendPortals message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onSetPortal(SetPortal message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onTeleportPlayerToPortal(TeleportPlayerToPortal message);

	// Spawns
	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onGetSpawns(GetSpawns message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onSendSpawns(SendSpawns message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onSendToProxySpawn(SendToProxySpawn message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onSetSpawn(SetSpawn message);

	// Teleports
	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onPlayersDeathBackLocation(PlayersDeathBackLocation message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onPlayersTeleportBackLocation(PlayersTeleportBackLocation message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onSendPlayerBack(SendPlayerBack message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onTeleportToLocation(TeleportToLocation message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onTeleportToPlayer(TeleportToPlayer message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onToggleTeleports(ToggleTeleports message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onTPAHereRequest(TPAHereRequest message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onTPAll(TPAll message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onTPAReply(TPAReply message);

	// Warps
	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onDeleteWarp(DeleteWarp message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onGetWarpsList(GetWarpsList message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onSetWarp(SetWarp message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onWarpPlayer(WarpPlayer message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onDiscoveryRequest(DiscoveryRequest message);

	/**
	 * Callback method which is called when the appropriate message is received.
	 * 
	 * @param message
	 *            the received message.
	 */
	void onDiscoveryHello(DiscoveryHello message);
}
