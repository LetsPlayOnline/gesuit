package eu.letsplayonline.gesuit.common.messages.portals;

import java.util.Objects;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class ListPortals extends Message {
	private String playerName;

	public ListPortals(String targetServer, String name, String serial,
			String playerName) {
		super(targetServer, name, serial);
		this.playerName = playerName;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;
		ListPortals that = (ListPortals) o;
		return Objects.equals(playerName, that.playerName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), playerName);
	}

	@Override
	public String toString() {
		return "ListPortals{" + "playerName='" + playerName + '\'' + '}';
	}
}
