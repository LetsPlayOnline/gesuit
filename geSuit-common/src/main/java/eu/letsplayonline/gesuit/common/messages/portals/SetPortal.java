package eu.letsplayonline.gesuit.common.messages.portals;

import eu.letsplayonline.gesuit.common.messages.Message;
import eu.letsplayonline.gesuit.common.messages.Portal;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class SetPortal extends Message {
	String playerName;
	Portal newPortal;

	public SetPortal() {
	}

	public SetPortal(String targetServer, String name, String serial,
			String playerName, Portal newPortal) {
		super(targetServer, name, serial);
		this.playerName = playerName;
		this.newPortal = newPortal;
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public Portal getNewPortal() {
		return this.newPortal;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setNewPortal(Portal newPortal) {
		this.newPortal = newPortal;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof SetPortal))
			return false;
		final SetPortal other = (SetPortal) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$playerName = this.playerName;
		final Object other$playerName = other.playerName;
		if (this$playerName == null ? other$playerName != null
				: !this$playerName.equals(other$playerName))
			return false;
		final Object this$newPortal = this.newPortal;
		final Object other$newPortal = other.newPortal;
		if (this$newPortal == null ? other$newPortal != null : !this$newPortal
				.equals(other$newPortal))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $playerName = this.playerName;
		result = result * PRIME
				+ ($playerName == null ? 0 : $playerName.hashCode());
		final Object $newPortal = this.newPortal;
		result = result * PRIME
				+ ($newPortal == null ? 0 : $newPortal.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof SetPortal;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.portals.SetPortal(playerName="
				+ this.playerName + ", newPortal=" + this.newPortal + ")";
	}
}
