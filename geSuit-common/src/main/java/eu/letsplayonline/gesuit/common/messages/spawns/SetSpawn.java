package eu.letsplayonline.gesuit.common.messages.spawns;

import eu.letsplayonline.gesuit.common.messages.Location;
import eu.letsplayonline.gesuit.common.messages.Message;
import eu.letsplayonline.gesuit.common.messages.SpawnType;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class SetSpawn extends Message {
	private String playerName;
	private Location spawnLocation;
	private boolean exists;
	private SpawnType spawnType;

	public SetSpawn(String targetServer, String sender, String serial,
			String playerName, Location SpawnLocation, boolean Exists,
			SpawnType spawnType) {
		super(targetServer, sender, serial);
		this.playerName = playerName;
		this.spawnLocation = SpawnLocation;
		this.exists = Exists;
		this.spawnType = spawnType;
	}

	public SetSpawn() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public Location getSpawnLocation() {
		return this.spawnLocation;
	}

	public boolean isExists() {
		return this.exists;
	}

	public SpawnType getSpawnType() {
		return this.spawnType;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setSpawnLocation(Location SpawnLocation) {
		this.spawnLocation = SpawnLocation;
	}

	public void setExists(boolean Exists) {
		this.exists = Exists;
	}

	public void setSpawnType(SpawnType spawnType) {
		this.spawnType = spawnType;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof SetSpawn))
			return false;
		final SetSpawn other = (SetSpawn) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$playerName = this.playerName;
		final Object other$playerName = other.playerName;
		if (this$playerName == null ? other$playerName != null
				: !this$playerName.equals(other$playerName))
			return false;
		final Object this$SpawnLocation = this.spawnLocation;
		final Object other$SpawnLocation = other.spawnLocation;
		if (this$SpawnLocation == null ? other$SpawnLocation != null
				: !this$SpawnLocation.equals(other$SpawnLocation))
			return false;
		if (this.exists != other.exists)
			return false;
		final Object this$spawnType = this.spawnType;
		final Object other$spawnType = other.spawnType;
		if (this$spawnType == null ? other$spawnType != null : !this$spawnType
				.equals(other$spawnType))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $playerName = this.playerName;
		result = result * PRIME
				+ ($playerName == null ? 0 : $playerName.hashCode());
		final Object $SpawnLocation = this.spawnLocation;
		result = result * PRIME
				+ ($SpawnLocation == null ? 0 : $SpawnLocation.hashCode());
		result = result * PRIME + (this.exists ? 79 : 97);
		final Object $spawnType = this.spawnType;
		result = result * PRIME
				+ ($spawnType == null ? 0 : $spawnType.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof SetSpawn;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.spawns.SetSpawn(playerName="
				+ this.playerName
				+ ", spawnLocation="
				+ this.spawnLocation
				+ ", exists="
				+ this.exists
				+ ", spawnType="
				+ this.spawnType
				+ ")";
	}
}
