package eu.letsplayonline.gesuit.common.messages;

public class Spawn {

	private String spawnName;
	private Location spawnLocation;

	public Spawn() {

	}

	public Spawn(String spawnName, Location spawnLocation) {
		super();
		this.spawnName = spawnName;
		this.spawnLocation = spawnLocation;
	}

	public String getSpawnName() {
		return spawnName;
	}

	public void setSpawnName(String spawnName) {
		this.spawnName = spawnName;
	}

	public Location getSpawnLocation() {
		return spawnLocation;
	}

	public void setSpawnLocation(Location spawnLocation) {
		this.spawnLocation = spawnLocation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((spawnLocation == null) ? 0 : spawnLocation.hashCode());
		result = prime * result
				+ ((spawnName == null) ? 0 : spawnName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Spawn other = (Spawn) obj;
		if (spawnLocation == null) {
			if (other.spawnLocation != null)
				return false;
		} else if (!spawnLocation.equals(other.spawnLocation))
			return false;
		if (spawnName == null) {
			if (other.spawnName != null)
				return false;
		} else if (!spawnName.equals(other.spawnName))
			return false;
		return true;
	}

}
