package eu.letsplayonline.gesuit.common;

import java.util.LinkedList;
import java.util.List;

import org.apache.camel.builder.ProxyBuilder;
import org.apache.camel.builder.RouteBuilder;

import eu.letsplayonline.gesuit.common.interfaces.Receiver;
import eu.letsplayonline.gesuit.common.interfaces.SendMessages;
import eu.letsplayonline.gesuit.common.messages.Message;
import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryHello;
import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryRequest;
import eu.letsplayonline.gesuit.common.messages.homes.DeleteHome;
import eu.letsplayonline.gesuit.common.messages.homes.GetHomesList;
import eu.letsplayonline.gesuit.common.messages.homes.SendPlayerHome;
import eu.letsplayonline.gesuit.common.messages.homes.SetPlayerHome;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortal;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortalPhy;
import eu.letsplayonline.gesuit.common.messages.portals.ListPortals;
import eu.letsplayonline.gesuit.common.messages.portals.RequestPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SendPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SetPortal;
import eu.letsplayonline.gesuit.common.messages.portals.TeleportPlayerToPortal;
import eu.letsplayonline.gesuit.common.messages.spawns.GetSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendToProxySpawn;
import eu.letsplayonline.gesuit.common.messages.spawns.SetSpawn;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersDeathBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersTeleportBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.SendPlayerBack;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAHereRequest;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAReply;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAll;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToPlayer;
import eu.letsplayonline.gesuit.common.messages.teleports.ToggleTeleports;
import eu.letsplayonline.gesuit.common.messages.warps.DeleteWarp;
import eu.letsplayonline.gesuit.common.messages.warps.GetWarpsList;
import eu.letsplayonline.gesuit.common.messages.warps.SetWarp;
import eu.letsplayonline.gesuit.common.messages.warps.WarpPlayer;

public class MessageHandler extends RouteBuilder {

	private final String host;
	private final String port;
	private final String queue;
	private static final List<Receiver> receivers = new LinkedList<>();
	private static SendMessages proxy = null;
	private static String serverName;
	private static boolean receiveAll = false;

	public MessageHandler(String host, String port, String queueName,
			String serverName, boolean receiveAll) {
		this.host = host;
		this.port = port;
		this.queue = queueName;
		MessageHandler.serverName = serverName;
		MessageHandler.receiveAll = receiveAll;
	}

	@Override
	public void configure() throws Exception {
		from(
				"rabbitmq://" + host + ":" + port + "/" + queue
						+ "?exchangeType=topic&concurrentConsumers=10")
				.unmarshal().xstream().bean(MessageInput.class, "output(*)");
		from("direct:start")
				.marshal()
				.xstream()
				.to("rabbitmq://" + host + ":" + port + "/" + queue
						+ "?exchangeType=topic");
		proxy = new ProxyBuilder(getContext()).endpoint("direct:start").build(
				SendMessages.class);

		// We only need this because for some reason, RabbitMQ tries to
		// convert
		// the finished object back to byte[]... Bug?
		getContext().getTypeConverterRegistry().addTypeConverters(
				new MessageConverter());
	}

	public SendMessages get() {
		return proxy;
	}

	public static void addReceiver(Receiver receiver) {
		synchronized (receivers) {
			receivers.add(receiver);
		}
	}

	public static class MessageInput {

		private static boolean shouldHandle(Message message) {
			boolean selfSent = serverName.equals(message.getSender());
			boolean broadCast = message.getTargetServer() == null;
			boolean forUs = broadCast
					|| serverName.equals(message.getTargetServer());
			boolean handle = !selfSent && (forUs || receiveAll);
			if (handle) {
				System.out.println("Received message from "
						+ message.getSender() + " to "
						+ message.getTargetServer() + " with serial "
						+ message.getSerial());
			}
			return handle;
		}

		// Homes
		public static void output(DeleteHome message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onDeleteHome(message);
				}
		}

		public static void output(GetHomesList message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onGetHomesList(message);
				}
		}

		public static void output(SendPlayerHome message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onSendPlayerHome(message);
				}
		}

		public static void output(SetPlayerHome message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onSetPlayerHome(message);
				}
		}

		// Portals
		public static void output(DeletePortal message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onDeletePortal(message);
				}
		}

		public static void output(DeletePortalPhy message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onDeletePortalPhy(message);
				}
		}

		public static void output(ListPortals message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onListPortals(message);
				}
		}

		public static void output(RequestPortals message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onRequestPortals(message);
				}
		}

		public static void output(SendPortals message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onSendPortals(message);
				}
		}

		public static void output(SetPortal message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onSetPortal(message);
				}
		}

		public static void output(TeleportPlayerToPortal message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onTeleportPlayerToPortal(message);
				}
		}

		// Spawns
		public static void output(GetSpawns message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onGetSpawns(message);
				}
		}

		public static void output(SendSpawns message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onSendSpawns(message);
				}
		}

		public static void output(SendToProxySpawn message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onSendToProxySpawn(message);
				}
		}

		public static void output(SetSpawn message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onSetSpawn(message);
				}
		}

		// Teleports

		public static void output(PlayersDeathBackLocation message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onPlayersDeathBackLocation(message);
				}
		}

		public static void output(PlayersTeleportBackLocation message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onPlayersTeleportBackLocation(message);
				}
		}

		public static void output(SendPlayerBack message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onSendPlayerBack(message);
				}
		}

		public static void output(TeleportToLocation message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onTeleportToLocation(message);
				}
		}

		public static void output(TeleportToPlayer message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onTeleportToPlayer(message);
				}
		}

		public static void output(ToggleTeleports message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onToggleTeleports(message);
				}
		}

		public static void output(TPAHereRequest message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onTPAHereRequest(message);
				}
		}

		public static void output(TPAll message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onTPAll(message);
				}
		}

		public static void output(TPAReply message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onTPAReply(message);
				}
		}

		// Warps
		public static void output(DeleteWarp message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onDeleteWarp(message);
				}
		}

		public static void output(GetWarpsList message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onGetWarpsList(message);
				}
		}

		public static void output(SetWarp message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onSetWarp(message);
				}
		}

		public static void output(WarpPlayer message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onWarpPlayer(message);
				}
		}

		public static void output(DiscoveryHello message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onDiscoveryHello(message);
				}
		}

		public static void output(DiscoveryRequest message) {
			if (shouldHandle(message))
				for (Receiver receiver : receivers) {
					receiver.onDiscoveryRequest(message);
				}
		}
	}
}
