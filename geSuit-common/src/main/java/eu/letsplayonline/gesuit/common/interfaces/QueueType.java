package eu.letsplayonline.gesuit.common.interfaces;

/**
 * Enumeration containing the types of queues we are communicating on.
 * 
 * @author jfrank
 *
 */
public enum QueueType {
	/** Queue for teleport datagrams. */
	TELEPORTS,
	/** Queue for spawn datagrams. */
	SPAWNS,
	/** Queue for warp datagrams. */
	WARPS,
	/** Queue for portal datagrams. */
	PORTALS,
	/** Queue for homes datagrams. */
	HOMES,
	/** Queue for discovery datagrams. */
}
