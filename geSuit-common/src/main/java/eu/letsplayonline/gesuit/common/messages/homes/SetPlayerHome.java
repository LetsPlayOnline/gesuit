package eu.letsplayonline.gesuit.common.messages.homes;

import eu.letsplayonline.gesuit.common.messages.Location;
import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class SetPlayerHome extends Message {
	private String playerName;
	private int serverLimit;
	private int globalLimit;
	private String homeName;
	private Location homeLocation;

	public SetPlayerHome(String targetServer, String sender, String serial,
			String PlayerName, int ServerLimit, int GlobalLimit,
			String HomeName, Location HomeLocation) {
		super(targetServer, sender, serial);
		this.playerName = PlayerName;
		this.serverLimit = ServerLimit;
		this.globalLimit = GlobalLimit;
		this.homeName = HomeName;
		this.homeLocation = HomeLocation;
	}

	public SetPlayerHome() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public int getServerLimit() {
		return this.serverLimit;
	}

	public int getGlobalLimit() {
		return this.globalLimit;
	}

	public String getHomeName() {
		return this.homeName;
	}

	public Location getHomeLocation() {
		return this.homeLocation;
	}

	public void setPlayerName(String PlayerName) {
		this.playerName = PlayerName;
	}

	public void setServerLimit(int ServerLimit) {
		this.serverLimit = ServerLimit;
	}

	public void setGlobalLimit(int GlobalLimit) {
		this.globalLimit = GlobalLimit;
	}

	public void setHomeName(String HomeName) {
		this.homeName = HomeName;
	}

	public void setHomeLocation(Location HomeLocation) {
		this.homeLocation = HomeLocation;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof SetPlayerHome))
			return false;
		final SetPlayerHome other = (SetPlayerHome) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$PlayerName = this.playerName;
		final Object other$PlayerName = other.playerName;
		if (this$PlayerName == null ? other$PlayerName != null
				: !this$PlayerName.equals(other$PlayerName))
			return false;
		if (this.serverLimit != other.serverLimit)
			return false;
		if (this.globalLimit != other.globalLimit)
			return false;
		final Object this$HomeName = this.homeName;
		final Object other$HomeName = other.homeName;
		if (this$HomeName == null ? other$HomeName != null : !this$HomeName
				.equals(other$HomeName))
			return false;
		final Object this$HomeLocation = this.homeLocation;
		final Object other$HomeLocation = other.homeLocation;
		if (this$HomeLocation == null ? other$HomeLocation != null
				: !this$HomeLocation.equals(other$HomeLocation))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $PlayerName = this.playerName;
		result = result * PRIME
				+ ($PlayerName == null ? 0 : $PlayerName.hashCode());
		result = result * PRIME + this.serverLimit;
		result = result * PRIME + this.globalLimit;
		final Object $HomeName = this.homeName;
		result = result * PRIME
				+ ($HomeName == null ? 0 : $HomeName.hashCode());
		final Object $HomeLocation = this.homeLocation;
		result = result * PRIME
				+ ($HomeLocation == null ? 0 : $HomeLocation.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof SetPlayerHome;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.homes.SetPlayerHome(playerName="
				+ this.playerName
				+ ", serverLimit="
				+ this.serverLimit
				+ ", globalLimit="
				+ this.globalLimit
				+ ", homeName="
				+ this.homeName + ", homeLocation=" + this.homeLocation + ")";
	}
}
