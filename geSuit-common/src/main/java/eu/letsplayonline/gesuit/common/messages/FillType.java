package eu.letsplayonline.gesuit.common.messages;

/**
 * This enum defines the types of portal filling.
 *
 * @author Julian Fölsch, jfrank
 */
public enum FillType {
	/** Water source blocks. */
	WATER,
	/** Lava source blocks. */
	LAVA,
	/** Air (invisible). */
	AIR,
	/** Spider cobwebs. */
	WEB,
	/** Sugar cane. */
	SUGAR_CANE,
	/** Nether portal. */
	PORTAL,
	/** End portal. */
	END_PORTAL
}
