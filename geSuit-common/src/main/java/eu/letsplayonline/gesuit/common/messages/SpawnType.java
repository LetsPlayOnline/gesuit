package eu.letsplayonline.gesuit.common.messages;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public enum SpawnType {
    SERVER, WORLD, PROXY, NEW_PLAYER
}
