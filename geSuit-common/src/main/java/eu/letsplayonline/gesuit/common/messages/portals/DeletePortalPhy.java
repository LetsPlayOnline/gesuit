package eu.letsplayonline.gesuit.common.messages.portals;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class DeletePortalPhy extends Message {
	String portalName;

	public DeletePortalPhy(String targetServer, String name, String serial,
			String portalName) {
		super(targetServer, name, serial);
		this.portalName = portalName;
	}

	public DeletePortalPhy() {
	}

	public String getPortalName() {
		return this.portalName;
	}

	public void setPortalName(String portalName) {
		this.portalName = portalName;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof DeletePortalPhy))
			return false;
		final DeletePortalPhy other = (DeletePortalPhy) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$portalName = this.portalName;
		final Object other$portalName = other.portalName;
		if (this$portalName == null ? other$portalName != null
				: !this$portalName.equals(other$portalName))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $portalName = this.portalName;
		result = result * PRIME
				+ ($portalName == null ? 0 : $portalName.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof DeletePortalPhy;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.portals.DeletePortalPhy(portalName="
				+ this.portalName + ")";
	}
}
