package eu.letsplayonline.gesuit.common.messages.homes;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class GetHomesList extends Message {
	private String playerName;

	public GetHomesList(String targetServer, String sender, String serial,
			String playerName) {
		super(targetServer, sender, serial);
		this.playerName = playerName;
	}

	public GetHomesList() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof GetHomesList))
			return false;
		final GetHomesList other = (GetHomesList) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$playerName = this.playerName;
		final Object other$playerName = other.playerName;
		if (this$playerName == null ? other$playerName != null
				: !this$playerName.equals(other$playerName))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $playerName = this.playerName;
		result = result * PRIME
				+ ($playerName == null ? 0 : $playerName.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof GetHomesList;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.homes.GetHomesList(playerName="
				+ this.playerName + ")";
	}
}
