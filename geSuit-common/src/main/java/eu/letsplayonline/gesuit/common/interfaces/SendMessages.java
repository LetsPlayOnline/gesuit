package eu.letsplayonline.gesuit.common.interfaces;

import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryHello;
import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryRequest;
import eu.letsplayonline.gesuit.common.messages.homes.DeleteHome;
import eu.letsplayonline.gesuit.common.messages.homes.GetHomesList;
import eu.letsplayonline.gesuit.common.messages.homes.SendPlayerHome;
import eu.letsplayonline.gesuit.common.messages.homes.SetPlayerHome;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortal;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortalPhy;
import eu.letsplayonline.gesuit.common.messages.portals.ListPortals;
import eu.letsplayonline.gesuit.common.messages.portals.RequestPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SendPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SetPortal;
import eu.letsplayonline.gesuit.common.messages.portals.TeleportPlayerToPortal;
import eu.letsplayonline.gesuit.common.messages.spawns.GetSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendToProxySpawn;
import eu.letsplayonline.gesuit.common.messages.spawns.SetSpawn;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersDeathBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersTeleportBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.SendPlayerBack;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAHereRequest;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAReply;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAll;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToPlayer;
import eu.letsplayonline.gesuit.common.messages.teleports.ToggleTeleports;
import eu.letsplayonline.gesuit.common.messages.warps.DeleteWarp;
import eu.letsplayonline.gesuit.common.messages.warps.GetWarpsList;
import eu.letsplayonline.gesuit.common.messages.warps.SetWarp;
import eu.letsplayonline.gesuit.common.messages.warps.WarpPlayer;

/**
 * Interface that provides the send methods for the different message types.
 * Needs to implement send message for each implementation (instead of just the
 * generic Message base class) since it's not actually implemented by us but
 * used as a proxy in the camel route.
 * 
 * @author jfrank
 *
 */
public interface SendMessages {

	// Homes
	/**
	 * Send message for the defined message type.
	 * 
	 * @param deleteHome
	 *            the message to send.
	 */
	void send(DeleteHome deleteHome);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param getHomesList
	 *            the message to send.
	 */
	void send(GetHomesList getHomesList);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param sendPlayerHome
	 *            the message to send.
	 */
	void send(SendPlayerHome sendPlayerHome);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param setPlayerHome
	 *            the message to send.
	 */
	void send(SetPlayerHome setPlayerHome);

	// Portals
	/**
	 * Send message for the defined message type.
	 * 
	 * @param deletePortal
	 *            the message to send.
	 */
	void send(DeletePortal deletePortal);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param deletePortalPhys
	 *            the message to send.
	 */
	void send(DeletePortalPhy deletePortalPhys);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param listPortals
	 *            the message to send.
	 */
	void send(ListPortals listPortals);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param requestPortals
	 *            the message to send.
	 */
	void send(RequestPortals requestPortals);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param sendPortals
	 *            the message to send.
	 */
	void send(SendPortals sendPortals);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param setPortal
	 *            the message to send.
	 */
	void send(SetPortal setPortal);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param teleportPlayerToPortal
	 *            the message to send.
	 */
	void send(TeleportPlayerToPortal teleportPlayerToPortal);

	// Spawns
	/**
	 * Send message for the defined message type.
	 * 
	 * @param getSpawns
	 *            the message to send.
	 */
	void send(GetSpawns getSpawns);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param sendSpawns
	 *            the message to send.
	 */
	void send(SendSpawns sendSpawns);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param sendToProxySpawn
	 *            the message to send.
	 */
	void send(SendToProxySpawn sendToProxySpawn);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param setSpawn
	 *            the message to send.
	 */
	void send(SetSpawn setSpawn);

	// Teleports
	/**
	 * Send message for the defined message type.
	 * 
	 * @param playersDeathBackLocation
	 *            the message to send.
	 */
	void send(PlayersDeathBackLocation playersDeathBackLocation);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param playersTeleportBackLocation
	 *            the message to send.
	 */
	void send(PlayersTeleportBackLocation playersTeleportBackLocation);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param sendPlayerBack
	 *            the message to send.
	 */
	void send(SendPlayerBack sendPlayerBack);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param teleportToLocation
	 *            the message to send.
	 */
	void send(TeleportToLocation teleportToLocation);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param teleportToPlayer
	 *            the message to send.
	 */
	void send(TeleportToPlayer teleportToPlayer);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param toggleTeleports
	 *            the message to send.
	 */
	void send(ToggleTeleports toggleTeleports);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param tpaHereRequest
	 *            the message to send.
	 */
	void send(TPAHereRequest tpaHereRequest);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param tpAll
	 *            the message to send.
	 */
	void send(TPAll tpAll);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param tpaReply
	 *            the message to send.
	 */
	void send(TPAReply tpaReply);

	// Warps
	/**
	 * Send message for the defined message type.
	 * 
	 * @param deleteWarp
	 *            the message to send.
	 */
	void send(DeleteWarp deleteWarp);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param getWarpsList
	 *            the message to send.
	 */
	void send(GetWarpsList getWarpsList);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param setWarp
	 *            the message to send.
	 */
	void send(SetWarp setWarp);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param warpPlayer
	 *            the message to send.
	 */
	void send(WarpPlayer warpPlayer);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param discoveryHello
	 *            the message to send.
	 */
	void send(DiscoveryHello discoveryHello);

	/**
	 * Send message for the defined message type.
	 * 
	 * @param discoveryRequest
	 *            the message to send.
	 */
	void send(DiscoveryRequest discoveryRequest);
}
