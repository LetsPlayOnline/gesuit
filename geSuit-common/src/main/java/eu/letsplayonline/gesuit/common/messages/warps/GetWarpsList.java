package eu.letsplayonline.gesuit.common.messages.warps;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class GetWarpsList extends Message {
	private String playerName;
	private boolean showServerWarps;
	private boolean showGlobalWarps;
	private boolean showHiddenWarps;
	private boolean bypass;

	public GetWarpsList(String targetServer, String sender, String serial,
			String PlayerName, boolean ShowServerWarps,
			boolean ShowGlobalWarps, boolean ShowHiddenWarps, boolean Bypass) {
		super(targetServer, sender, serial);
		this.playerName = PlayerName;
		this.showServerWarps = ShowServerWarps;
		this.showGlobalWarps = ShowGlobalWarps;
		this.showHiddenWarps = ShowHiddenWarps;
		this.bypass = Bypass;
	}

	public GetWarpsList() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public boolean isShowServerWarps() {
		return this.showServerWarps;
	}

	public boolean isShowGlobalWarps() {
		return this.showGlobalWarps;
	}

	public boolean isShowHiddenWarps() {
		return this.showHiddenWarps;
	}

	public boolean isBypass() {
		return this.bypass;
	}

	public void setPlayerName(String PlayerName) {
		this.playerName = PlayerName;
	}

	public void setShowServerWarps(boolean ShowServerWarps) {
		this.showServerWarps = ShowServerWarps;
	}

	public void setShowGlobalWarps(boolean ShowGlobalWarps) {
		this.showGlobalWarps = ShowGlobalWarps;
	}

	public void setShowHiddenWarps(boolean ShowHiddenWarps) {
		this.showHiddenWarps = ShowHiddenWarps;
	}

	public void setBypass(boolean Bypass) {
		this.bypass = Bypass;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof GetWarpsList))
			return false;
		final GetWarpsList other = (GetWarpsList) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$PlayerName = this.playerName;
		final Object other$PlayerName = other.playerName;
		if (this$PlayerName == null ? other$PlayerName != null
				: !this$PlayerName.equals(other$PlayerName))
			return false;
		if (this.showServerWarps != other.showServerWarps)
			return false;
		if (this.showGlobalWarps != other.showGlobalWarps)
			return false;
		if (this.showHiddenWarps != other.showHiddenWarps)
			return false;
		if (this.bypass != other.bypass)
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $PlayerName = this.playerName;
		result = result * PRIME
				+ ($PlayerName == null ? 0 : $PlayerName.hashCode());
		result = result * PRIME + (this.showServerWarps ? 79 : 97);
		result = result * PRIME + (this.showGlobalWarps ? 79 : 97);
		result = result * PRIME + (this.showHiddenWarps ? 79 : 97);
		result = result * PRIME + (this.bypass ? 79 : 97);
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof GetWarpsList;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.warps.GetWarpsList(playerName="
				+ this.playerName
				+ ", showServerWarps="
				+ this.showServerWarps
				+ ", showGlobalWarps="
				+ this.showGlobalWarps
				+ ", showHiddenWarps="
				+ this.showHiddenWarps
				+ ", bypass="
				+ this.bypass + ")";
	}
}
