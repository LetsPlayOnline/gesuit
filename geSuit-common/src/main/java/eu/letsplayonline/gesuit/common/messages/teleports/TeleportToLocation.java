package eu.letsplayonline.gesuit.common.messages.teleports;

import eu.letsplayonline.gesuit.common.messages.Location;
import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class TeleportToLocation extends Message {
	private String playerName;
	private Location target;

	public TeleportToLocation(String targetServer, String sender,
			String serial, String playerName, Location target) {
		super(targetServer, sender, serial);
		this.playerName = playerName;
		this.target = target;
	}

	public TeleportToLocation() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public Location getTarget() {
		return this.target;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setTarget(Location target) {
		this.target = target;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof TeleportToLocation))
			return false;
		final TeleportToLocation other = (TeleportToLocation) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$playerName = this.playerName;
		final Object other$playerName = other.playerName;
		if (this$playerName == null ? other$playerName != null
				: !this$playerName.equals(other$playerName))
			return false;
		final Object this$target = this.target;
		final Object other$target = other.target;
		if (this$target == null ? other$target != null : !this$target
				.equals(other$target))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $playerName = this.playerName;
		result = result * PRIME
				+ ($playerName == null ? 0 : $playerName.hashCode());
		final Object $target = this.target;
		result = result * PRIME + ($target == null ? 0 : $target.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof TeleportToLocation;
	}

	public String toString() {
		return "TeleportToLocation. playerName="
				+ this.playerName + ", target=" + this.target + " " + super.toString();
	}
}
