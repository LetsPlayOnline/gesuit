package eu.letsplayonline.gesuit.common;

import org.apache.camel.Converter;
import org.apache.camel.TypeConverters;

import eu.letsplayonline.gesuit.common.messages.Message;

public class MessageConverter implements TypeConverters {

	@Converter
	public byte[] toByteArray(Message message) {
		return new byte[] {};
	}
}
