package eu.letsplayonline.gesuit.common;

import java.util.LinkedList;
import java.util.List;

import eu.letsplayonline.gesuit.common.interfaces.Factory;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.Location;
import eu.letsplayonline.gesuit.common.messages.Spawn;
import eu.letsplayonline.gesuit.common.messages.portals.SendPortals;
import eu.letsplayonline.gesuit.common.messages.spawns.GetSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendSpawns;

public class Strafarbeiter {
	private static IMessageManager mm; 
	private static long nowSpawns= 0;
	private static long nowPortals = 0;
	private static long then = 0;
	public static void main(String[] args) throws Exception {
		mm = new MessageManager();
		mm.setBrokerEndpoint("localhost", "5672");
		mm.setQueue("geSuit");
		mm.setServerName("lobby0");
		mm.receiveAll();
		mm.registerReceiver(new Blah());
		mm.initialize();
		System.out.println("Initialized");

		nowSpawns= System.currentTimeMillis();
		nowPortals = nowSpawns;
//		mm.get().send(Factory.newSendSpawns("lobby0", spawns));
//		mm.get().send(Factory.newGetSpawns());
		Location loc = new Location(1, 2, 3, "String", 0, 0);
		mm.get().send(Factory.newPlayersTeleportBackLocation("lobby0", "DJGummikuh", loc));
		while (true) {
			Thread.sleep(1000);
		}
		/**
		 * RequestSpawns
		 * RequestPortals
		 * 
		 */
	}

	private static class Blah extends ReceiverBase {
		@Override
		public void onSendSpawns(SendSpawns message) {
			System.out.println(System.currentTimeMillis() - nowSpawns);
//			mm.get().send(Factory.newGetSpawns());
		}
		@Override
		public void onGetSpawns(GetSpawns message) {
		}
		@Override
		public void onSendPortals(SendPortals message) {
			System.out.println(System.currentTimeMillis() - nowPortals);
		}
	}
}
