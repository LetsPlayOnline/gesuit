package eu.letsplayonline.gesuit.common.messages.homes;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class SendPlayerHome extends Message {
	private String playerName;
	private String homeName;

	public SendPlayerHome(String targetServer, String sender, String serial,
			String playerName, String homeName) {
		super(targetServer, sender, serial);
		this.playerName = playerName;
		this.homeName = homeName;
	}

	public SendPlayerHome() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public String getHomeName() {
		return this.homeName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof SendPlayerHome))
			return false;
		final SendPlayerHome other = (SendPlayerHome) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$playerName = this.playerName;
		final Object other$playerName = other.playerName;
		if (this$playerName == null ? other$playerName != null
				: !this$playerName.equals(other$playerName))
			return false;
		final Object this$homeName = this.homeName;
		final Object other$homeName = other.homeName;
		if (this$homeName == null ? other$homeName != null : !this$homeName
				.equals(other$homeName))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $playerName = this.playerName;
		result = result * PRIME
				+ ($playerName == null ? 0 : $playerName.hashCode());
		final Object $homeName = this.homeName;
		result = result * PRIME
				+ ($homeName == null ? 0 : $homeName.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof SendPlayerHome;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.homes.SendPlayerHome(playerName="
				+ this.playerName + ", homeName=" + this.homeName + ")";
	}
}
