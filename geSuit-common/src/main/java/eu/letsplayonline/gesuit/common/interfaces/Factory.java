package eu.letsplayonline.gesuit.common.interfaces;

import java.util.List;
import java.util.UUID;

import eu.letsplayonline.gesuit.common.messages.Location;
import eu.letsplayonline.gesuit.common.messages.Portal;
import eu.letsplayonline.gesuit.common.messages.PortalType;
import eu.letsplayonline.gesuit.common.messages.Spawn;
import eu.letsplayonline.gesuit.common.messages.SpawnType;
import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryHello;
import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryRequest;
import eu.letsplayonline.gesuit.common.messages.homes.DeleteHome;
import eu.letsplayonline.gesuit.common.messages.homes.GetHomesList;
import eu.letsplayonline.gesuit.common.messages.homes.SendPlayerHome;
import eu.letsplayonline.gesuit.common.messages.homes.SetPlayerHome;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortal;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortalPhy;
import eu.letsplayonline.gesuit.common.messages.portals.ListPortals;
import eu.letsplayonline.gesuit.common.messages.portals.RequestPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SendPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SetPortal;
import eu.letsplayonline.gesuit.common.messages.portals.TeleportPlayerToPortal;
import eu.letsplayonline.gesuit.common.messages.spawns.GetSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendToProxySpawn;
import eu.letsplayonline.gesuit.common.messages.spawns.SetSpawn;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersDeathBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersTeleportBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.SendPlayerBack;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAHereRequest;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAReply;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAll;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToPlayer;
import eu.letsplayonline.gesuit.common.messages.teleports.ToggleTeleports;
import eu.letsplayonline.gesuit.common.messages.warps.DeleteWarp;
import eu.letsplayonline.gesuit.common.messages.warps.GetWarpsList;
import eu.letsplayonline.gesuit.common.messages.warps.SetWarp;
import eu.letsplayonline.gesuit.common.messages.warps.WarpPlayer;

/**
 * This class serves as a factory for message datagrams. It already sets the
 * common fields source and message id to ensure consistent message routing.
 * 
 * @author jfrank
 *
 */
public class Factory {

	/**
	 * The name of this server instance.
	 */
	private static String source;

	/**
	 * Setter for the name of this server instance.
	 * 
	 * @param source
	 */
	public static void setSource(String source) {
		Factory.source = source;
	}

	/**
	 * Method to generate a message id.
	 * 
	 * @return a unique message id.
	 */
	private static String uid() {
		return UUID.randomUUID().toString();
	}

	// Homes
	/**
	 * Create a new message datagram to trigger the deletion of a home.
	 * 
	 * @param targetServer
	 *            the server where the home should be deleted on.
	 * @param playerName
	 *            the name of the player that wants to delete a home.
	 * @param homeName
	 *            the name of the home.
	 * @return the message object.
	 */
	public static DeleteHome newDeleteHome(String targetServer,
			String playerName, String homeName) {
		return new DeleteHome(targetServer, source, uid(), playerName, homeName);
	}

	/**
	 * Create a new message datagram to trigger the retrieval of the list of all
	 * homes.
	 * 
	 * @param targetServer
	 *            the server to query.
	 * @param playerName
	 *            the name of the player.
	 * @return the message object.
	 */
	public static GetHomesList newGetHomesList(String targetServer,
			String playerName) {
		return new GetHomesList(targetServer, source, uid(), playerName);
	}

	/**
	 * Create a new message datagram to trigger a teleport of a player to a
	 * given home.
	 * 
	 * @param targetServer
	 *            the target server.
	 * @param playerName
	 *            the player name.
	 * @param homeName
	 *            the name of the target home.
	 * @return the message object.
	 */
	public static SendPlayerHome newSendPlayerHome(String targetServer,
			String playerName, String homeName) {
		return new SendPlayerHome(targetServer, source, uid(), playerName,
				homeName);
	}

	/**
	 * Create a new message datagram to trigger the creation of a new player
	 * home.
	 * 
	 * @param targetServer
	 *            the server where the home is located.
	 * @param playerName
	 *            the name of the player.
	 * @param serverLimit
	 *            the maximum number of homes on that server.
	 * @param globalLimit
	 *            the maximum number of homes globally.
	 * @param homeName
	 *            the name of the home.
	 * @param homeLocation
	 *            the location of the home.
	 * @return the message object.
	 */
	public static SetPlayerHome newSetPlayerHome(String targetServer,
			String playerName, int serverLimit, int globalLimit,
			String homeName, Location homeLocation) {
		return new SetPlayerHome(targetServer, source, uid(), playerName,
				serverLimit, globalLimit, homeName, homeLocation);
	}

	// Portals
	/**
	 * Creates a new message datagram that triggers a server to evaluate the
	 * deletion of a portal. Will be replied to with the DeletePortalPhy
	 * datagram.
	 * 
	 * @param targetServer
	 *            the server to delete the portal on.
	 * @param playerName
	 *            the name of the player requesting the deletion.
	 * @param portalName
	 *            the portal to delete.
	 * @return the message object.
	 */
	public static DeletePortal newDeletePortal(String targetServer,
			String playerName, String portalName) {
		return new DeletePortal(targetServer, source, uid(), playerName,
				portalName);
	}

	/**
	 * Creates a new message datagram that triggers the actual deletion of the
	 * portal. Second stage after the DeletePortal datagram.
	 * 
	 * @param targetServer
	 *            the target server.
	 * @param portalName
	 *            the name of the deleted portal.
	 * @return the message object.
	 */
	public static DeletePortalPhy newDeletePortalPhy(String targetServer,
			String portalName) {
		return new DeletePortalPhy(targetServer, source, uid(), portalName);
	}

	/**
	 * Creates a new message datagram that triggers the creation of a message to
	 * the requesting players containing all configured portals.
	 * 
	 * @param targetServer
	 *            the server to send this request to.
	 * @param playerName
	 *            the name of the requesting player.
	 * @return the message object.
	 */
	public static ListPortals newListPortals(String targetServer,
			String playerName) {
		return new ListPortals(targetServer, source, uid(), playerName);
	}

	/**
	 * Creates a new broadcast message datagram that triggers all listening
	 * servers to send their portals to the broadcaster.
	 * 
	 * @return the message object.
	 */
	public static RequestPortals newRequestPortals() {
		return new RequestPortals(null, source, uid());
	}

	/**
	 * Creates a new message datagram that contains all portals for this server.
	 * Is the reply to the GetPortals datagram.
	 * 
	 * @param targetServer
	 *            the server to send the spawns to.
	 * @param portalList
	 *            the list of spawns.
	 * @return the message object.
	 */
	public static SendPortals newSendPortals(String targetServer,
			List<Portal> portalList) {
		return new SendPortals(targetServer, source, uid(), portalList);
	}

	/**
	 * Creates a new message datagram that triggers the creation of a new
	 * portal.
	 * 
	 * @param targetServer
	 *            the server to create the portal on.
	 * @param playerName
	 *            the name of the player.
	 * @param newPortal
	 *            the Portal object.
	 * @return the message object.
	 */
	public static SetPortal newSetPortal(String targetServer,
			String playerName, Portal newPortal) {
		return new SetPortal(targetServer, source, uid(), playerName, newPortal);
	}

	/**
	 * Creates a new message datagram that triggers the teleportation of a
	 * player to a given portal.
	 * 
	 * @param targetServer
	 *            the server to send the player to.
	 * @param playerName
	 *            the name of the player to port.
	 * @param portalType
	 *            the type of the portal.
	 * @param destinationName
	 *            the name of the portal to port to.
	 * @return the message object.
	 */
	public static TeleportPlayerToPortal newTeleportPlayerToPortal(
			String targetServer, String playerName, PortalType portalType,
			String destinationName) {
		return new TeleportPlayerToPortal(targetServer, source, uid(),
				playerName, portalType, destinationName);
	}

	// Spawns
	/**
	 * Creates a broadcast message datagram that requests all servers listening
	 * to send their spawns to the broadcaster.
	 * 
	 * @return the message object.
	 */
	public static GetSpawns newGetSpawns() {
		return new GetSpawns(null, source, uid());
	}

	/**
	 * Creates a message datagram that contains all the spawns on this server.
	 * Is the reply to the GetSpawns datagram.
	 * 
	 * @param targetServer
	 *            the server to send the spawns to.
	 * @param spawns
	 *            a list of spawns.
	 * @return the message object.
	 */
	public static SendSpawns newSendSpawns(String targetServer,
			List<Spawn> spawns) {
		return new SendSpawns(targetServer, source, uid(), spawns);
	}

	/**
	 * Creates a new message datagram to trigger the teleport of a player to the
	 * proxy spawn.
	 * 
	 * @param targetServer
	 *            the server to spawn to.
	 * @param playerName
	 *            the name of the player to spawn.
	 * @param silent
	 *            whether or not the spawn should be messaged to the player.
	 * @return
	 */
	public static SendToProxySpawn newSendToProxySpawn(String targetServer,
			String playerName, boolean silent) {
		return new SendToProxySpawn(targetServer, source, uid(), playerName,
				silent);
	}

	/**
	 * Creates a new message datagram to trigger the creation of a new server
	 * spawn.
	 * 
	 * @param targetServer
	 *            the server to create the spawn on.
	 * @param playerName
	 *            the name of the creating player.
	 * @param spawnLocation
	 *            the location of the spawn.
	 * @param exists
	 *            whether or not the spawn already exists (update) or not
	 *            (create)
	 * @param spawnType
	 *            the type of the spawn.
	 * @return the message object.
	 */
	public static SetSpawn newSetSpawn(String targetServer, String playerName,
			Location spawnLocation, boolean exists, SpawnType spawnType) {
		return new SetSpawn(targetServer, source, uid(), playerName,
				spawnLocation, exists, spawnType);
	}

	// Teleports
	/**
	 * Creates a new message datagram to trigger the store of a new player death
	 * back location.
	 * 
	 * @param targetServer
	 *            the server of the back location.
	 * @param playerName
	 *            the name of the player.
	 * @param target
	 *            the back location.
	 * @return the message object.
	 */
	public static PlayersDeathBackLocation newPlayersDeathBackLocation(
			String targetServer, String playerName, Location target) {
		return new PlayersDeathBackLocation(targetServer, source, uid(),
				playerName, target);
	}

	/**
	 * Creates a new message datagram to trigger the store of a new player
	 * teleport back location.
	 * 
	 * @param targetServer
	 *            the server of the back location.
	 * @param playerName
	 *            the name of the player.
	 * @param target
	 *            the back location.
	 * @return the message object.
	 */
	public static PlayersTeleportBackLocation newPlayersTeleportBackLocation(
			String targetServer, String playerName, Location target) {
		return new PlayersTeleportBackLocation(targetServer, source, uid(),
				playerName, target);
	}

	/**
	 * Creates a new message datagram to trigger the teleportation of a player
	 * to his last back location.
	 * 
	 * @param targetServer
	 *            the server to teleport to.
	 * @param playerName
	 *            the name of the server to teleport.
	 * @param death
	 *            whether to teleport to the last death back point.
	 * @param teleport
	 *            whether to teleport to the last teleport back point.
	 * @return the message object.
	 */
	public static SendPlayerBack newSendPlayerBack(String targetServer,
			String playerName, boolean death, boolean teleport) {
		return new SendPlayerBack(targetServer, source, uid(), playerName,
				death, teleport);
	}

	/**
	 * Creates a new message datagram to trigger the teleportation of a player
	 * to a location
	 * 
	 * @param targetServer
	 *            the server to teleport to.
	 * @param playerName
	 *            the name of the player to teleport.
	 * @param target
	 *            the target location to teleport to.
	 * @return the message object.
	 */
	public static TeleportToLocation newTeleportToLocation(String targetServer,
			String playerName, Location target) {
		return new TeleportToLocation(targetServer, source, uid(), playerName,
				target);
	}

	/**
	 * Creates a new message datagram to trigger the teleportation of a player
	 * to another player.
	 * 
	 * @param targetServer
	 *            the server to send the message to.
	 * @param commandSender
	 *            the person sending this command.
	 * @param playerToTeleport
	 *            the player to teleport.
	 * @param playerToTeleportTo
	 *            the player to teleport to.
	 * @param silent
	 *            whether or not this should be shown to the players.
	 * @param bypass
	 *            whether or not to bypass permissions.
	 * @return the message object.
	 */
	public static TeleportToPlayer newTeleportToPlayer(String targetServer,
			String commandSender, String playerToTeleport,
			String playerToTeleportTo, boolean silent, boolean bypass) {
		return new TeleportToPlayer(targetServer, source, uid(), commandSender,
				playerToTeleport, playerToTeleportTo, silent, bypass);
	}

	/**
	 * Creates a new message datagram to trigger the state-switch on whether or
	 * not incoming tpa requests are activated for a player.
	 * 
	 * @param targetServer
	 *            the server to send the message to.
	 * @param playerName
	 *            the name of the affected player.
	 * @return the message object.
	 */
	public static ToggleTeleports newToggleTeleports(String targetServer,
			String playerName) {
		return new ToggleTeleports(targetServer, source, uid(), playerName);
	}

	/**
	 * Creates a new message datagram to trigger the sending of a /tpahere or
	 * /tpa request.
	 * 
	 * @param targetServer
	 *            the server to send the message to.
	 * @param playerName
	 *            the player that sends the request.
	 * @param targetName
	 *            the player that receives the request.
	 * @param meToHim
	 *            which direction to teleport. <br>
	 *            true - /tp the requesting player to the target player. <br>
	 *            false - /tphere the target player to the requesting player.
	 * @return the message object.
	 */
	public static TPAHereRequest newTPAHereRequest(String targetServer,
			String playerName, String targetName, boolean meToHim) {
		return new TPAHereRequest(targetServer, source, uid(), playerName,
				targetName, meToHim);
	}

	/**
	 * Creates a new message datagram to trigger the teleportation of every
	 * player to a given target player.
	 * 
	 * @param targetServer
	 *            the server to send the request to.
	 * @param commandSender
	 *            the player that initiates this command.
	 * @param playerTarget
	 *            the player to teleport the other players to.
	 * @param affectedServer
	 *            from which server to teleport the players. If this is null,
	 *            players from EVERY server are teleported.
	 * @return the message object.
	 */
	public static TPAll newTPAll(String targetServer, String commandSender,
			String playerTarget, String affectedServer) {
		return new TPAll(targetServer, source, uid(), commandSender,
				playerTarget, affectedServer);
	}

	/**
	 * Creates a new message datagram to trigger the sending of a reply to the
	 * TPA request.
	 * 
	 * @param targetServer
	 *            the server to send the reply to.
	 * @param accept
	 *            whether or not the request was accepted.
	 * @param playerName
	 *            the name of the replying player.
	 * @return the message object.
	 */
	public static TPAReply newTPAReply(String targetServer, boolean accept,
			String playerName) {
		return new TPAReply(targetServer, source, uid(), accept, playerName);
	}

	// Warps
	/**
	 * Creates a new message datagram to trigger the removal of a warp point.
	 * 
	 * @param targetServer
	 *            the server to delete the warp on.
	 * @param playerName
	 *            the name of the requesting player.
	 * @param warpName
	 *            the name of the warp to delete.
	 * @return the message object.
	 */
	public static DeleteWarp newDeleteWarp(String targetServer,
			String playerName, String warpName) {
		return new DeleteWarp(targetServer, source, uid(), playerName, warpName);
	}

	/**
	 * Creates a new message datagram to trigger the retrieval of the warps.
	 * 
	 * @param targetServer
	 *            the server to query.
	 * @param playerName
	 *            the name of the requesting player.
	 * @param showServerWarps
	 *            whether or not to show non-global warps.
	 * @param showGlobalWarps
	 *            whether or not to show global warps.
	 * @param showHiddenWarps
	 *            whether or not to show hidden warps.
	 * @param bypass
	 *            whether or not to bypass permissions.
	 * @return the message object.
	 */
	public static GetWarpsList newGetWarpsList(String targetServer,
			String playerName, boolean showServerWarps,
			boolean showGlobalWarps, boolean showHiddenWarps, boolean bypass) {
		return new GetWarpsList(targetServer, source, uid(), playerName,
				showServerWarps, showGlobalWarps, showHiddenWarps, bypass);
	}

	/**
	 * Creates a message datagram to trigger the creation of a new warp point.
	 * 
	 * @param targetServer
	 *            the server the warp point is created on.
	 * @param playerName
	 *            the name of the player (for logging).
	 * @param warpName
	 *            the name of the warp.
	 * @param warpLocation
	 *            the location of the warp.
	 * @param global
	 *            whether or not it's globally valid.
	 * @param hidden
	 *            whether or not it's shown on the warps list.
	 * @return the message object.
	 */
	public static SetWarp newSetWarp(String targetServer, String playerName,
			String warpName, Location warpLocation, boolean global,
			boolean hidden) {
		return new SetWarp(targetServer, source, uid(), playerName, warpName,
				warpLocation, global, hidden);
	}

	/**
	 * Creates a new message datagram to trigger warping a player to a target
	 * warp.
	 * 
	 * @param targetServer
	 *            the server to warp to.
	 * @param playerName
	 *            the name of the player.
	 * @param warpName
	 *            the name of the warp.
	 * @param bypass
	 *            whether or not to bypass permission checking.
	 * @return the message object.
	 */
	public static WarpPlayer newWarpPlayer(String targetServer,
			String playerName, String warpName, boolean bypass) {
		return new WarpPlayer(targetServer, source, uid(), playerName,
				warpName, bypass);
	}

	/**
	 * Creates a new message datagram to trigger all leafs to send their
	 * connection information to bungee.
	 * 
	 * @return the message object.
	 */
	public static DiscoveryRequest newDiscoveryRequest() {
		return new DiscoveryRequest(null, source, uid());
	}

	/**
	 * Creates a new message datagram to report the own host and port to bungee.
	 * 
	 * @param host
	 *            the host of this server.
	 * @param port
	 *            the port of this server.
	 * @return the message object.
	 */
	public static DiscoveryHello newDiscoveryHello(String host, int port) {
		return new DiscoveryHello(null, source, uid(), host, port);
	}
}
