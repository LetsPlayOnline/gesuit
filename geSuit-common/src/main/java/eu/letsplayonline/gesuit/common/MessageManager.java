package eu.letsplayonline.gesuit.common;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;

import eu.letsplayonline.gesuit.common.interfaces.Factory;
import eu.letsplayonline.gesuit.common.interfaces.Receiver;
import eu.letsplayonline.gesuit.common.interfaces.SendMessages;

public class MessageManager implements IMessageManager {

	private String host = null;
	private String port = null;

	private MessageHandler handler = null;
	private CamelContext context = null;
	private String queueName = null;
	private String serverName = null;
	private boolean receiveAll = false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.letsplayonline.gesuit.common.IMessageManager#setBrokerEndpoint(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public void setBrokerEndpoint(String host, String port) {
		this.host = host;
		this.port = port;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.letsplayonline.gesuit.common.IMessageManager#setInQueue(java.lang.
	 * String)
	 */
	@Override
	public void setQueue(String queueName) {
		this.queueName = queueName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.letsplayonline.gesuit.common.IMessageManager#registerReceiver(eu.
	 * letsplayonline.gesuit.common.interfaces.Receiver)
	 */
	@Override
	public void registerReceiver(Receiver receiver) {
		MessageHandler.addReceiver(receiver);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.letsplayonline.gesuit.common.IMessageManager#homes()
	 */
	@Override
	public SendMessages get() {
		return handler.get();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.letsplayonline.gesuit.common.IMessageManager#initialize()
	 */
	@Override
	public void initialize() {
		if (context == null) {
			Factory.setSource(serverName);
			handler = new MessageHandler(host, port, queueName, serverName,
					receiveAll);
			context = new DefaultCamelContext();
			try {
				context.addRoutes(handler);
				context.start();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.letsplayonline.gesuit.common.IMessageManager#shutdown()
	 */
	@Override
	public void shutdown() {
		try {
			context.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		handler = null;
		context = null;
	}

	@Override
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	@Override
	public void receiveAll() {
		receiveAll = true;

	}
}
