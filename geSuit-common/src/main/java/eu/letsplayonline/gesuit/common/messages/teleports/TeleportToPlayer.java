package eu.letsplayonline.gesuit.common.messages.teleports;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class TeleportToPlayer extends Message {
	private String commandSender;
	private String playerToTeleport;
	private String playerToTeleportTo;
	private boolean silent;
	private boolean bypass;

	public TeleportToPlayer(String targetServer, String sender, String serial,
			String commandSender, String playerToTeleport,
			String playerToTeleportTo, boolean silent, boolean bypass) {
		super(targetServer, sender, serial);
		this.commandSender = commandSender;
		this.playerToTeleport = playerToTeleport;
		this.playerToTeleportTo = playerToTeleportTo;
		this.silent = silent;
		this.bypass = bypass;
	}

	public TeleportToPlayer() {
	}

	public String getCommandSender() {
		return this.commandSender;
	}

	public String getPlayerToTeleport() {
		return this.playerToTeleport;
	}

	public String getPlayerToTeleportTo() {
		return this.playerToTeleportTo;
	}

	public boolean isSilent() {
		return this.silent;
	}

	public boolean isBypass() {
		return this.bypass;
	}

	public void setCommandSender(String commandSender) {
		this.commandSender = commandSender;
	}

	public void setPlayerToTeleport(String playerToTeleport) {
		this.playerToTeleport = playerToTeleport;
	}

	public void setPlayerToTeleportTo(String playerToTeleportTo) {
		this.playerToTeleportTo = playerToTeleportTo;
	}

	public void setSilent(boolean silent) {
		this.silent = silent;
	}

	public void setBypass(boolean bypass) {
		this.bypass = bypass;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof TeleportToPlayer))
			return false;
		final TeleportToPlayer other = (TeleportToPlayer) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$commandSender = this.commandSender;
		final Object other$commandSender = other.commandSender;
		if (this$commandSender == null ? other$commandSender != null
				: !this$commandSender.equals(other$commandSender))
			return false;
		final Object this$playerToTeleport = this.playerToTeleport;
		final Object other$playerToTeleport = other.playerToTeleport;
		if (this$playerToTeleport == null ? other$playerToTeleport != null
				: !this$playerToTeleport.equals(other$playerToTeleport))
			return false;
		final Object this$playerToTeleportTo = this.playerToTeleportTo;
		final Object other$playerToTeleportTo = other.playerToTeleportTo;
		if (this$playerToTeleportTo == null ? other$playerToTeleportTo != null
				: !this$playerToTeleportTo.equals(other$playerToTeleportTo))
			return false;
		if (this.silent != other.silent)
			return false;
		if (this.bypass != other.bypass)
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $commandSender = this.commandSender;
		result = result * PRIME
				+ ($commandSender == null ? 0 : $commandSender.hashCode());
		final Object $playerToTeleport = this.playerToTeleport;
		result = result
				* PRIME
				+ ($playerToTeleport == null ? 0 : $playerToTeleport.hashCode());
		final Object $playerToTeleportTo = this.playerToTeleportTo;
		result = result
				* PRIME
				+ ($playerToTeleportTo == null ? 0 : $playerToTeleportTo
						.hashCode());
		result = result * PRIME + (this.silent ? 79 : 97);
		result = result * PRIME + (this.bypass ? 79 : 97);
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof TeleportToPlayer;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.teleports.TeleportToPlayer(commandSender="
				+ this.commandSender
				+ ", playerToTeleport="
				+ this.playerToTeleport
				+ ", playerToTeleportTo="
				+ this.playerToTeleportTo
				+ ", silent="
				+ this.silent
				+ ", bypass=" + this.bypass + ")";
	}
}
