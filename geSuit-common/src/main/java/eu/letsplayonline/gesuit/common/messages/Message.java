package eu.letsplayonline.gesuit.common.messages;

/**
 * Message base class. Can not directly be instanced and is only meant to be
 * subclassed.
 *
 * @author Julian Fölsch, jfrank
 */
public abstract class Message {
	/** The sender server of the message. */
	String sender;
	/**
	 * The messages serial number. <br>
	 * The serial number must be unique (e.g UUID) so that a message that is
	 * meant to be a reply to this message can relate to the original message by
	 * providing the original messages serial number.
	 */
	String serial;
	/**
	 * The intended target for this message. If null or "", the message is
	 * considered to be a broadcast.
	 */
	private String targetServer;

	/**
	 * Constructor initializing all fields.
	 * 
	 * @param targetServer
	 *            the target server.
	 * @param sender
	 *            the sender server.
	 * @param serial
	 *            the serial number.
	 */
	public Message(String targetServer, String sender, String serial) {
		this.sender = sender;
		this.serial = serial;
		this.targetServer = targetServer;
	}

	/**
	 * Default constructor for deserialization.
	 */
	public Message() {
	}

	/**
	 * Getter for the target server.
	 * 
	 * @return the target server.
	 */
	public String getTargetServer() {
		return targetServer;
	}

	/**
	 * Getter for the sender server.
	 * 
	 * @return the sender server.
	 */
	public String getSender() {
		return this.sender;
	}

	/**
	 * Getter for the serial number.
	 * 
	 * @return the serial number.
	 */
	public String getSerial() {
		return this.serial;
	}

	/**
	 * Setter for the target server.
	 * 
	 * @param targetServer
	 *            the target server.
	 */
	public void setTargetServer(String targetServer) {
		this.targetServer = targetServer;
	}

	/**
	 * Setter for the sender server.
	 * 
	 * @param sender
	 *            the sender server.
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * Setter for the serial number.
	 * 
	 * @param serial
	 *            the serial number.
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof Message))
			return false;
		final Message other = (Message) o;
		final Object this$sender = this.sender;
		final Object other$sender = other.sender;
		if (this$sender == null ? other$sender != null : !this$sender
				.equals(other$sender))
			return false;
		final Object this$serial = this.serial;
		final Object other$serial = other.serial;
		if (this$serial == null ? other$serial != null : !this$serial
				.equals(other$serial))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $sender = this.sender;
		result = result * PRIME + ($sender == null ? 0 : $sender.hashCode());
		final Object $serial = this.serial;
		result = result * PRIME + ($serial == null ? 0 : $serial.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "sender=" + this.sender + ", serial=" + this.serial
				+ ", targetServer=" + targetServer;
	}
}
