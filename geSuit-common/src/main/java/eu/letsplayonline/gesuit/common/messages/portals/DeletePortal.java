package eu.letsplayonline.gesuit.common.messages.portals;

import java.util.Objects;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class DeletePortal extends Message {
	private String playerName;
	private String portalName;

	public DeletePortal(String targetServer, String name, String serial,
			String playerName, String portalName) {
		super(targetServer, name, serial);
		this.playerName = playerName;
		this.portalName = portalName;
	}

	public String getPortalName() {
		return portalName;
	}

	public void setPortalName(String portalName) {
		this.portalName = portalName;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;
		DeletePortal that = (DeletePortal) o;
		return Objects.equals(playerName, that.playerName)
				&& Objects.equals(portalName, that.portalName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), playerName, portalName);
	}

	@Override
	public String toString() {
		return "DeletePortal{" + "playerName='" + playerName + '\''
				+ ", portalName='" + portalName + '\'' + '}';
	}
}
