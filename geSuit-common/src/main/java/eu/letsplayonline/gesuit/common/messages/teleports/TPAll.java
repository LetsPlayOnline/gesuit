package eu.letsplayonline.gesuit.common.messages.teleports;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class TPAll extends Message {
	private String commandSender;
	private String playerTarget;
	private String affectedServer;

	public TPAll(String targetServer, String sender, String serial,
			String commandSender, String playerTarget, String affectedServer) {
		super(targetServer, sender, serial);
		this.commandSender = commandSender;
		this.playerTarget = playerTarget;
		this.affectedServer = affectedServer;
	}

	public TPAll() {
	}

	public String getAffectedServer() {
		return affectedServer;
	}
	public void setAffectedServer(String affectedServer) {
		this.affectedServer = affectedServer;
	}
	public String getCommandSender() {
		return this.commandSender;
	}

	public String getPlayerTarget() {
		return this.playerTarget;
	}

	public void setCommandSender(String commandSender) {
		this.commandSender = commandSender;
	}

	public void setPlayerTarget(String playerTarget) {
		this.playerTarget = playerTarget;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof TPAll))
			return false;
		final TPAll other = (TPAll) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$commandSender = this.commandSender;
		final Object other$commandSender = other.commandSender;
		if (this$commandSender == null ? other$commandSender != null
				: !this$commandSender.equals(other$commandSender))
			return false;
		final Object this$playerTarget = this.playerTarget;
		final Object other$playerTarget = other.playerTarget;
		if (this$playerTarget == null ? other$playerTarget != null
				: !this$playerTarget.equals(other$playerTarget))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $commandSender = this.commandSender;
		result = result * PRIME
				+ ($commandSender == null ? 0 : $commandSender.hashCode());
		final Object $playerTarget = this.playerTarget;
		result = result * PRIME
				+ ($playerTarget == null ? 0 : $playerTarget.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof TPAll;
	}

	public String toString() {
		return "TPAll: commandSender="
				+ this.commandSender
				+ ", playerTarget="
				+ this.playerTarget
				+ ", affectedServer="
				+ affectedServer + " " + super.toString();
	}
}
