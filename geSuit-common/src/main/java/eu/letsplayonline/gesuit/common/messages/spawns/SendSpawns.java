package eu.letsplayonline.gesuit.common.messages.spawns;

import java.util.List;

import eu.letsplayonline.gesuit.common.messages.Message;
import eu.letsplayonline.gesuit.common.messages.Spawn;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class SendSpawns extends Message {
	private List<Spawn> spawns;

	public SendSpawns(String targetServer, String sender, String serial,
			List<Spawn> spawns) {
		super(targetServer, sender, serial);
		this.spawns = spawns;
	}

	public SendSpawns() {
	}

	public List<Spawn> getSpawns() {
		return this.spawns;
	}

	public void setSpawns(List<Spawn> spawns) {
		this.spawns = spawns;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof SendSpawns))
			return false;
		final SendSpawns other = (SendSpawns) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$spawns = this.spawns;
		final Object other$spawns = other.spawns;
		if (this$spawns == null ? other$spawns != null : !this$spawns
				.equals(other$spawns))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $spawns = this.spawns;
		result = result * PRIME + ($spawns == null ? 0 : $spawns.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof SendSpawns;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.spawns.SendSpawns(spawns="
				+ this.spawns + ")";
	}
}
