package eu.letsplayonline.gesuit.common.messages.spawns;

import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class GetSpawns extends Message {
	public GetSpawns(String targetServer, String sender, String serial) {
		super(targetServer, sender, serial);
	}

	public GetSpawns() {

	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof GetSpawns))
			return false;
		final GetSpawns other = (GetSpawns) o;
		if (!other.canEqual((Object) this))
			return false;
		return true;
	}

	public int hashCode() {
		int result = 1;
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof GetSpawns;
	}

	public String toString() {
		return "eu.letsplayonline.geSuit.common.messages.spawns.GetSpawns()";
	}
}
