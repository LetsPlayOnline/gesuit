package eu.letsplayonline.gesuit.common.messages.teleports;

import eu.letsplayonline.gesuit.common.messages.Location;
import eu.letsplayonline.gesuit.common.messages.Message;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class PlayersTeleportBackLocation extends Message {
	private String playerName;
	private Location target;

	public PlayersTeleportBackLocation(String targetServer, String sender,
			String serial, String playerName, Location target) {
		super(targetServer, sender, serial);
		this.playerName = playerName;
		this.target = target;
	}

	public PlayersTeleportBackLocation() {
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public Location getTarget() {
		return this.target;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setTarget(Location target) {
		this.target = target;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof PlayersTeleportBackLocation))
			return false;
		final PlayersTeleportBackLocation other = (PlayersTeleportBackLocation) o;
		if (!other.canEqual((Object) this))
			return false;
		final Object this$playerNamen = this.playerName;
		final Object other$playerNamen = other.playerName;
		if (this$playerNamen == null ? other$playerNamen != null
				: !this$playerNamen.equals(other$playerNamen))
			return false;
		final Object this$target = this.target;
		final Object other$target = other.target;
		if (this$target == null ? other$target != null : !this$target
				.equals(other$target))
			return false;
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $playerNamen = this.playerName;
		result = result * PRIME
				+ ($playerNamen == null ? 0 : $playerNamen.hashCode());
		final Object $target = this.target;
		result = result * PRIME + ($target == null ? 0 : $target.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof PlayersTeleportBackLocation;
	}

	public String toString() {
		return "PlayersTeleportBackLocation! playerName="
				+ this.playerName + ", target=" + this.target + " " + super.toString();
	}
}
