package eu.letsplayonline.gesuit.common.interfaces;

import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryHello;
import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryRequest;
import eu.letsplayonline.gesuit.common.messages.homes.DeleteHome;
import eu.letsplayonline.gesuit.common.messages.homes.GetHomesList;
import eu.letsplayonline.gesuit.common.messages.homes.SendPlayerHome;
import eu.letsplayonline.gesuit.common.messages.homes.SetPlayerHome;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortal;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortalPhy;
import eu.letsplayonline.gesuit.common.messages.portals.ListPortals;
import eu.letsplayonline.gesuit.common.messages.portals.RequestPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SendPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SetPortal;
import eu.letsplayonline.gesuit.common.messages.portals.TeleportPlayerToPortal;
import eu.letsplayonline.gesuit.common.messages.spawns.GetSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendToProxySpawn;
import eu.letsplayonline.gesuit.common.messages.spawns.SetSpawn;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersDeathBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersTeleportBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.SendPlayerBack;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAHereRequest;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAReply;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAll;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToPlayer;
import eu.letsplayonline.gesuit.common.messages.teleports.ToggleTeleports;
import eu.letsplayonline.gesuit.common.messages.warps.DeleteWarp;
import eu.letsplayonline.gesuit.common.messages.warps.GetWarpsList;
import eu.letsplayonline.gesuit.common.messages.warps.SetWarp;
import eu.letsplayonline.gesuit.common.messages.warps.WarpPlayer;

/**
 * Basic implementation of the Receiver interface. Ignores all messages. That
 * way custom implementations only have to implement the listeners for the
 * messages they are interested in.
 * 
 * @author jfrank
 *
 */
public class ReceiverBase implements Receiver {

	@Override
	public void onDeleteHome(DeleteHome message) {

	}

	@Override
	public void onGetHomesList(GetHomesList message) {

	}

	@Override
	public void onSendPlayerHome(SendPlayerHome message) {

	}

	@Override
	public void onSetPlayerHome(SetPlayerHome message) {

	}

	@Override
	public void onDeletePortal(DeletePortal message) {

	}

	@Override
	public void onDeletePortalPhy(DeletePortalPhy message) {

	}

	@Override
	public void onListPortals(ListPortals message) {

	}

	@Override
	public void onRequestPortals(RequestPortals message) {

	}

	@Override
	public void onSendPortals(SendPortals message) {

	}

	@Override
	public void onSetPortal(SetPortal message) {

	}

	@Override
	public void onTeleportPlayerToPortal(TeleportPlayerToPortal message) {

	}

	@Override
	public void onGetSpawns(GetSpawns message) {

	}

	@Override
	public void onSendSpawns(SendSpawns message) {

	}

	@Override
	public void onSendToProxySpawn(SendToProxySpawn message) {

	}

	@Override
	public void onSetSpawn(SetSpawn message) {

	}

	@Override
	public void onPlayersDeathBackLocation(PlayersDeathBackLocation message) {

	}

	@Override
	public void onPlayersTeleportBackLocation(
			PlayersTeleportBackLocation message) {

	}

	@Override
	public void onSendPlayerBack(SendPlayerBack message) {

	}

	@Override
	public void onTeleportToLocation(TeleportToLocation message) {

	}

	@Override
	public void onTeleportToPlayer(TeleportToPlayer message) {

	}

	@Override
	public void onToggleTeleports(ToggleTeleports message) {

	}

	@Override
	public void onTPAHereRequest(TPAHereRequest message) {

	}

	@Override
	public void onTPAll(TPAll message) {

	}

	@Override
	public void onTPAReply(TPAReply message) {

	}

	@Override
	public void onDeleteWarp(DeleteWarp message) {

	}

	@Override
	public void onGetWarpsList(GetWarpsList message) {

	}

	@Override
	public void onSetWarp(SetWarp message) {

	}

	@Override
	public void onWarpPlayer(WarpPlayer message) {

	}

	@Override
	public void onDiscoveryRequest(DiscoveryRequest message) {

	}

	@Override
	public void onDiscoveryHello(DiscoveryHello message) {

	}

}
