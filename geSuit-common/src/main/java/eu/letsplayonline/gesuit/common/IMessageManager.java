package eu.letsplayonline.gesuit.common;

import eu.letsplayonline.gesuit.common.interfaces.Receiver;
import eu.letsplayonline.gesuit.common.interfaces.SendMessages;

public interface IMessageManager {

	void setBrokerEndpoint(String host, String port);

	void setQueue(String queueName);

	void setServerName(String serverName);

	void registerReceiver(Receiver receiver);

	SendMessages get();

	void initialize();

	void shutdown();
	
	void receiveAll();

}