package eu.letsplayonline.gesuit.common.messages;

import java.util.Objects;

/**
 * Created by Julian Fölsch on 07.11.15.
 *
 * @author Julian Fölsch
 */
public class Portal {
    private String portalName;
    private PortalType portalType;
    private String destinationName;
    private FillType fillType;
    private Location minLocation;
    private Location maxLocation;

    public Portal() {
    }

    public Portal(String portalName, PortalType portalType, String destinationName, FillType fillType, Location minLocation, Location maxLocation) {
        this.portalName = portalName;
        this.portalType = portalType;
        this.destinationName = destinationName;
        this.fillType = fillType;
        this.minLocation = minLocation;
        this.maxLocation = maxLocation;
    }

    public String getPortalName() {
        return portalName;
    }

    public void setPortalName(String portalName) {
        this.portalName = portalName;
    }

    public PortalType getPortalType() {
        return portalType;
    }

    public void setPortalType(PortalType portalType) {
        this.portalType = portalType;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public FillType getFillType() {
        return fillType;
    }

    public void setFillType(FillType fillType) {
        this.fillType = fillType;
    }

    public Location getMinLocation() {
        return minLocation;
    }

    public void setMinLocation(Location minLocation) {
        this.minLocation = minLocation;
    }

    public Location getMaxLocation() {
        return maxLocation;
    }

    public void setMaxLocation(Location maxLocation) {
        this.maxLocation = maxLocation;
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Portal portal = (Portal) o;
        return Objects.equals(portalName, portal.portalName) &&
                Objects.equals(portalType, portal.portalType) &&
                Objects.equals(destinationName, portal.destinationName) &&
                Objects.equals(fillType, portal.fillType) &&
                Objects.equals(minLocation, portal.minLocation) &&
                Objects.equals(maxLocation, portal.maxLocation);
    }

    @Override public int hashCode() {
        return Objects.hash(portalName, portalType, destinationName, fillType, minLocation, maxLocation);
    }

    @Override public String toString() {
        return "Portal{" +
                "portalName='" + portalName + '\'' +
                ", portalType=" + portalType +
                ", destinationName='" + destinationName + '\'' +
                ", fillType=" + fillType +
                ", minLocation=" + minLocation +
                ", maxLocation=" + maxLocation +
                '}';
    }
}
