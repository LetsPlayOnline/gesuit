package eu.letsplayonline.hub;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class HubCommands implements CommandExecutor {

	private Hub hub;
	
	public HubCommands(Hub hub) {
		this.hub = hub;
	}

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		if (args != null && args.length > 0) {
			switch (args[0].toLowerCase()) {
				case "reload":
					if (!hub.hasPermission(cs, "reload")) {
						return true;
					}
					
					hub.loadConfig();
					hub.getGameSelectorManager().loadItems();
					hub.getGameSelector().loadMenu();
					
					cs.sendMessage(ChatColor.GRAY + "[Hub] " + ChatColor.GREEN + "Config reloaded");
					return true;
				case "save":
					if (!hub.hasPermission(cs, "save")) {
						return true;
					}
					
					hub.saveConfig();
					hub.getGameSelectorManager().loadItems();
					cs.sendMessage(ChatColor.GRAY + "[Hub] " + ChatColor.GREEN + "Config saved");
					return true;
			}
		}

		return false;
	}
	
}
