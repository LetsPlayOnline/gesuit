package eu.letsplayonline.hub;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GameSelectorItem {
	private String name;
	private String warp;
	private Material material;
	private int amount;
	private short metadata;
	private String displayName;
	private List<String> lore;
	private int row;
	private int position;

	public GameSelectorItem(String name, String warp, Material material,
			int amount, short metadata, String displayName, List<String> lore,
			int row, int position) {
		this.name = name;
		this.warp = warp;
		this.material = material;
		this.amount = amount;
		this.metadata = metadata;
		this.displayName = displayName;
		this.lore = lore;
		this.row = row;
		this.position = position;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getWarp() {
		return warp;
	}

	public void setWarp(String value) {
		this.warp = value;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material value) {
		this.material = value;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int value) {
		this.amount = value;
	}

	public short getMetadata() {
		return metadata;
	}

	public void setMetadata(short value) {
		this.metadata = value;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String value) {
		this.displayName = value;
	}

	public List<String> getLore() {
		return lore;
	}

	public void setLore(List<String> value) {
		this.lore = value;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int value) {
		this.row = value;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int value) {
		this.position = value;
	}

	public ItemStack getItemStack() {
		ItemStack item = new ItemStack(material, amount, metadata);
		ItemMeta meta = item.getItemMeta();

		meta.setDisplayName(displayName.replaceAll("&", "§")
				.replaceAll("Â", ""));

		List<String> newLore = new ArrayList<>();
		for (String line : lore) {
			newLore.add(line.replaceAll("&", "§").replaceAll("Â", ""));
		}
		meta.setLore(newLore);

		item.setItemMeta(meta);
		return item;
	}

}
