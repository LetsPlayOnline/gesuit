package eu.letsplayonline.hub;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class HubListener implements Listener {

	private Hub hub;

	public HubListener(Hub hub) {
		this.hub = hub;
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerJoin(PlayerJoinEvent event) {
		if (!event.getPlayer().hasPermission("hub.exempt")) {
			event.getPlayer().getInventory().clear();
		}
	}

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		if (!event.isCancelled()
				&& !event.getPlayer().hasPermission("hub.exempt")) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(
					ChatColor.GRAY + "Hub: " + ChatColor.RED
							+ "You cannot drop items here!");
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		try {
			if (!event.isCancelled()
					&& !event.getView().getPlayer().hasPermission("hub.exempt")) {
				event.setCancelled(true);
			}
		} catch (NullPointerException e) {
			Bukkit.getLogger().log(Level.INFO, "Caught NPE");
		}
	}
}
