package eu.letsplayonline.hub.gesuit;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import eu.letsplayonline.gesuit.common.GeSuitCommon;
import eu.letsplayonline.gesuit.common.interfaces.Factory;
import eu.letsplayonline.gesuit.common.messages.PortalType;
import eu.letsplayonline.hub.Hub;

public class PlayerTeleporter {
	public static void teleportPlayer(Plugin plugin, final Player player,
			final String warp) {
		Bukkit.getScheduler().runTaskAsynchronously(Hub.instance,
				new Runnable() {

					@Override
					public void run() {
						try {
							GeSuitCommon
									.getInstance()
									.get()
									.send(Factory.newTeleportPlayerToPortal(
											null, player.getName(),
											PortalType.WARP, warp));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}
}
