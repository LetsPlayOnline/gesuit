package eu.letsplayonline.hub;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import eu.letsplayonline.hub.items.BecomePremium;
import eu.letsplayonline.hub.items.GameSelector;
import eu.letsplayonline.hub.items.PlayerVisibility;

public class Hub extends JavaPlugin {

	private GameSelectorItemManager selectorItemManager;
	private GameSelector selector;
	private PlayerVisibility visibility;
	private BecomePremium premium;
	private File configFile;
	public FileConfiguration config;

	public static JavaPlugin instance = null;

	@Override
	public void onEnable() {
		instance = this;
		loadConfig();

		if (config.getBoolean("enabled")) {
			selectorItemManager = new GameSelectorItemManager(this);
			selectorItemManager.loadItems();

			selector = new GameSelector(this);
			// visibility = new PlayerVisibility(this);
			premium = new BecomePremium(this);
			getServer().getMessenger().registerOutgoingPluginChannel(this,
					"geSuitPortals");
			getServer().getPluginManager().registerEvents(
					new HubListener(this), this);
			getServer().getPluginManager().registerEvents(selector, this);
			// getServer().getPluginManager().registerEvents(visibility, this);
			getServer().getPluginManager().registerEvents(premium, this);
			getCommand("lpohub").setExecutor(new HubCommands(this));

			getLogger().info(
					String.format("Enabled Hub v%s", getDescription()
							.getVersion()));
		}
	}

	@Override
	public void onDisable() {
		if (config.getBoolean("enabled")) {
			selectorItemManager.saveItems();
			selectorItemManager = null;

			selector = null;
			visibility = null;
			premium = null;

			saveConfig();
			getLogger().info(
					String.format("Disabled Hub v%s", getDescription()
							.getVersion()));
		}
	}

	public void loadConfig() {
		if (configFile == null) {
			configFile = new File(getDataFolder(), "config.yml");
		}

		config = YamlConfiguration.loadConfiguration(configFile);
		if (!configFile.exists()) {
			try {
				config = YamlConfiguration
						.loadConfiguration(new InputStreamReader(this
								.getResource("config.yml")));
				config.save(configFile);
			} catch (IOException ex) {
				getLogger().log(Level.SEVERE, "Failed to save config", ex);
			}
		}
	}

	@Override
	public void saveConfig() {
		// if (configFile.length() > 0) {
		// try {
		// config.save(configFile);
		// } catch (IOException ex) {
		// getLogger().log(Level.SEVERE, "Failed to save config", ex);
		// }
		// }
	}

	public GameSelectorItemManager getGameSelectorManager() {
		return selectorItemManager;
	}

	public GameSelector getGameSelector() {
		return selector;
	}

	public PlayerVisibility getPlayerVisibility() {
		return visibility;
	}

	protected boolean hasPermission(CommandSender cs, String permission) {
		if (!cs.hasPermission(String.format("hub.%s", permission))) {
			cs.sendMessage(ChatColor.GRAY + "[Hub] " + ChatColor.RED
					+ "You do not have permission to do that");
			return false;
		}
		return true;
	}
}
