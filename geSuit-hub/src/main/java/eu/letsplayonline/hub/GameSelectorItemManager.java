package eu.letsplayonline.hub;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;

public class GameSelectorItemManager {
	private Hub hub;
	private ConfigurationSection section;
	private Map<String, GameSelectorItem> items;

	public GameSelectorItemManager(Hub hub) {
		this.hub = hub;
		this.section = hub.config.getConfigurationSection("gameselector.games");
		this.items = new HashMap<>();
	}

	public boolean isItem(String name) {
		return items.containsKey(name);
	}

	public boolean isItem(int row, int pos) {
		for (GameSelectorItem item : items.values()) {
			if (item.getRow() == row && item.getPosition() == pos) {
				return true;
			}
		}
		return false;
	}

	public boolean isYouAreHereItem(int row, int pos) {
		return (hub.config.getInt("gameselector.youarehere.row") == row)
				&& (hub.config.getInt("gameselector.youarehere.position") == pos);
	}

	public GameSelectorItem getItem(String name) {
		if (items.containsKey(name)) {
			return items.get(name);
		} else {
			return null;
		}
	}

	public GameSelectorItem getItem(int row, int pos) {
		for (GameSelectorItem item : items.values()) {
			if (item.getRow() == row && item.getPosition() == pos) {
				return item;
			}
		}
		return null;
	}

	public Collection<GameSelectorItem> getItems() {
		return items.values();
	}

	public void setItem(String name, GameSelectorItem item) {
		items.put(name, item);
	}

	public void removeItem(String name) {
		if (items.containsKey(name)) {
			items.remove(name);
		}
	}

	public void loadItems() {
		hub.getLogger().info("Loading gameselector items...");
		if (section != null) {
			items.clear();
			for (String key : section.getKeys(false)) {
				ConfigurationSection subSec = section
						.getConfigurationSection(key);
				String warp = subSec.getString("warp");
				String material = subSec.getString("material");
				int amount = subSec.getInt("amount");
				Bukkit.getLogger().log(
						Level.INFO,
						"Key: " + key + " Warp: " + warp + " Material: "
								+ material + " Amount: " + amount);
				items.put(
						key,
						new GameSelectorItem(key, warp, Material
								.matchMaterial(material), amount,
								(short) subSec.getInt("metadata"), subSec
										.getString("displayname"), subSec
										.getStringList("lore"), subSec
										.getInt("row"), subSec
										.getInt("position")));
			}
			hub.getLogger().info(items.size() + " items loaded.");
		} else {
			hub.getLogger().warning("Failed to load items. Section is null.");
		}
	}

	public void saveItems() {
		hub.getLogger().info("Saving gameselector items...");
		hub.config.set("gameselector.games", null);
		for (Entry<String, GameSelectorItem> entry : items.entrySet()) {
			final String name = entry.getKey();
			final GameSelectorItem item = entry.getValue();
			hub.config.set("gameselector.games." + name + ".warp",
					item.getWarp());
			hub.config.set("gameselector.games." + name + ".material", item
					.getMaterial().toString());
			hub.config.set("gameselector.games." + name + ".amount",
					item.getAmount());
			hub.config.set("gameselector.games." + name + ".metadata",
					item.getMetadata());
			hub.config.set("gameselector.games." + name + ".displayname",
					item.getDisplayName());
			hub.config.set("gameselector.games." + name + ".lore",
					item.getLore());
			hub.config
					.set("gameselector.games." + name + ".row", item.getRow());
			hub.config.set("gameselector.games." + name + ".position",
					item.getPosition());
			hub.getLogger().info(String.format("Saved item %s", name));
		}
		hub.getLogger().info("Saved " + items.size() + " items.");
	}
}
