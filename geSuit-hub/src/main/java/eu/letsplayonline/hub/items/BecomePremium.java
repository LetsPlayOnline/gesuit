package eu.letsplayonline.hub.items;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import eu.letsplayonline.hub.Hub;

public class BecomePremium implements Listener {

	private final Hub hub;
	private final int slot;
	
	public BecomePremium(Hub hub) {
		this.hub = hub;
		this.slot = hub.config.getInt("becomepremium.slot");
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.hasItem() && event.getPlayer().getItemInHand() != null && event.getPlayer().getItemInHand().hasItemMeta() && event.getPlayer().getItemInHand().getItemMeta().hasDisplayName()
				&& event.getPlayer().getItemInHand().getItemMeta()
						.getDisplayName()
						.equals(ChatColor.YELLOW + "Become Premium")) {
			if (event.getPlayer().hasPermission("hub.usebecomepremium")) {
				event.getPlayer().sendMessage("");
				for (String message : hub.config.getStringList("becomepremium.messages")) {
					event.getPlayer().sendMessage(message.replaceAll("&", "§").replaceAll("Â", ""));
				}
				event.getPlayer().sendMessage("");
			} else {
				event.getPlayer()
						.sendMessage(
								ChatColor.GRAY
										+ "[Hub] "
										+ ChatColor.RED
										+ "You do not have permission to use this item");
			}
		}
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.getPlayer().getInventory().setItem(slot, getItemStack());
	}
	
	public ItemStack getItemStack() {
		ItemStack stack = new ItemStack(Material.DIAMOND);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(ChatColor.YELLOW + "Become Premium");
		stack.setItemMeta(meta);
		return stack;
	}
	
}
