package eu.letsplayonline.hub.items;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import eu.letsplayonline.common.util.IconMenu;
import eu.letsplayonline.common.util.IconMenu.OptionClickEvent;
import eu.letsplayonline.hub.GameSelectorItem;
import eu.letsplayonline.hub.Hub;
import eu.letsplayonline.hub.gesuit.PlayerTeleporter;

public class GameSelector implements Listener {

	private final Hub hub;
	private final int slot;
	private IconMenu menu;

	public GameSelector(final Hub hub) {
		this.hub = hub;
		this.slot = hub.config.getInt("gameselector.slot");
		this.loadMenu();
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.hasItem()
				&& event.getPlayer().getItemInHand() != null
				&& event.getPlayer().getItemInHand().hasItemMeta()
				&& event.getPlayer().getItemInHand().getItemMeta()
						.hasDisplayName()
				&& event.getPlayer().getItemInHand().getItemMeta()
						.getDisplayName()
						.equals(ChatColor.YELLOW + "Game Selector")) {
			if (event.getPlayer().hasPermission("hub.useselector")) {
				menu.openMenu(event.getPlayer());
			} else {
				event.getPlayer()
						.sendMessage(
								ChatColor.GRAY
										+ "[Hub] "
										+ ChatColor.RED
										+ "You do not have permission to use the game selector");
			}
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.getPlayer().getInventory().setItem(slot, getItemStack());
	}

	public void loadMenu() {
		if (menu != null) {
			this.menu = null;
		}

		this.menu = new IconMenu(hub, "Game Selector",
				hub.config.getInt("gameselector.rows"),
				new IconMenu.OptionClickEventHandler() {
					@Override
					public void onOptionClick(OptionClickEvent event) {
						if (hub.getGameSelectorManager().isYouAreHereItem(
								event.getRow().getRow(), event.getPosition())) {
							String warp = hub.config
									.getString("gameselector.youarehere.warp");
							if (warp != null) {
								PlayerTeleporter.teleportPlayer(hub,
										event.getPlayer(), warp);
							}
						} else if (hub.getGameSelectorManager().isItem(
								event.getRow().getRow(), event.getPosition())) {
							String warp = hub
									.getGameSelectorManager()
									.getItem(event.getRow().getRow(),
											event.getPosition()).getWarp();
							if (warp != null) {
								PlayerTeleporter.teleportPlayer(hub,
										event.getPlayer(), warp);
							}
						}
						event.setWillClose(true);
					}
				});

		List<String> youAreHereLore = new ArrayList<>();
		for (String line : hub.config
				.getStringList("gameselector.youarehere.lore")) {
			youAreHereLore.add(line.replaceAll("&", "§").replaceAll("Â", ""));
		}
		menu.addButton(
				menu.getRow(hub.config.getInt("gameselector.youarehere.row")),
				hub.config.getInt("gameselector.youarehere.position"),
				new ItemStack(Material.matchMaterial(hub.config
						.getString("gameselector.youarehere.material")),
						hub.config.getInt("gameselector.youarehere.amount"),
						(short) hub.config
								.getInt("gameselector.youarehere.metadata")),
				hub.config.getString("gameselector.youarehere.displayname")
						.replaceAll("&", "§").replaceAll("Â", ""),
				youAreHereLore);

		for (GameSelectorItem item : hub.getGameSelectorManager().getItems()) {
			hub.getLogger().info(
					"Adding item " + item.getName() + " to game selector menu");
			menu.addButton(menu.getRow(item.getRow()), item.getPosition(),
					item.getItemStack(), item.getDisplayName(), item.getLore());
		}
	}

	public ItemStack getItemStack() {
		ItemStack stack = new ItemStack(Material.COMPASS);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(ChatColor.YELLOW + "Game Selector");
		stack.setItemMeta(meta);
		return stack;
	}

}
