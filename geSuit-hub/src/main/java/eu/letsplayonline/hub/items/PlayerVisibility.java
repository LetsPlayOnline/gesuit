package eu.letsplayonline.hub.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import eu.letsplayonline.hub.Hub;

public class PlayerVisibility implements Listener {

	private final Hub hub;
	private final int slot;
	private Map<String, Long> cooldown;
	private List<String> hiding;

	public PlayerVisibility(Hub hub) {
		this.hub = hub;
		this.slot = hub.config.getInt("playervisibility.slot");
		this.cooldown = new HashMap<>();
		this.hiding = new ArrayList<>();
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(final PlayerInteractEvent event) {
		if (event.hasItem()
				&& event.getPlayer().getItemInHand() != null
				&& event.getPlayer().getItemInHand().hasItemMeta()
				&& event.getPlayer().getItemInHand().getItemMeta()
						.hasDisplayName()
				&& (event
						.getPlayer()
						.getItemInHand()
						.getItemMeta()
						.getDisplayName()
						.equals(ChatColor.YELLOW + "Player Visibility"
								+ ChatColor.GRAY + " - " + ChatColor.GREEN
								+ "Visible") || event
						.getPlayer()
						.getItemInHand()
						.getItemMeta()
						.getDisplayName()
						.equals(ChatColor.YELLOW + "Player Visibility"
								+ ChatColor.GRAY + " - " + ChatColor.RED
								+ "Hidden"))) {
			if (event.getPlayer().hasPermission("hub.useplayervisibility")) {
				if (event
						.getPlayer()
						.getItemInHand()
						.getItemMeta()
						.getDisplayName()
						.equals(ChatColor.YELLOW + "Player Visibility"
								+ ChatColor.GRAY + " - " + ChatColor.GREEN
								+ "Visible")) {
					event.setCancelled(true);
					if (!handleCooldown(event.getPlayer())) {
						event.getPlayer()
								.getInventory()
								.setItem(
										event.getPlayer().getInventory()
												.getHeldItemSlot(),
										getItemStack(false));
						synchronized (hiding) {
							hiding.add(event.getPlayer().getName());
						}
						for (Player pl : Bukkit.getOnlinePlayers()) {
							if (!pl.hasPermission("hub.alwaysvisible")) {
								event.getPlayer().hidePlayer(pl);
							}
						}
						event.getPlayer().sendMessage(
								ChatColor.GRAY + "Player Visibility: "
										+ ChatColor.GREEN
										+ "Players are now hidden");
					} else {
						event.getPlayer()
								.getInventory()
								.setItem(
										event.getPlayer().getInventory()
												.getHeldItemSlot(),
										getItemStack(true));
					}
				} else if (event
						.getPlayer()
						.getItemInHand()
						.getItemMeta()
						.getDisplayName()
						.equals(ChatColor.YELLOW + "Player Visibility"
								+ ChatColor.GRAY + " - " + ChatColor.RED
								+ "Hidden")) {
					event.setCancelled(true);
					if (!handleCooldown(event.getPlayer())) {
						event.getPlayer()
								.getInventory()
								.setItem(
										event.getPlayer().getInventory()
												.getHeldItemSlot(),
										getItemStack(true));
						synchronized (hiding) {
							if (hiding.contains(event.getPlayer().getName())) {
								hiding.remove(event.getPlayer().getName());
							}
						}
						for (Player pl : Bukkit.getOnlinePlayers()) {
							if (!event.getPlayer().canSee(pl)) {
								event.getPlayer().showPlayer(pl);
							}
						}
						event.getPlayer().sendMessage(
								ChatColor.GRAY + "Player Visibility: "
										+ ChatColor.GREEN
										+ "Players are now visible");
					} else {
						event.getPlayer()
								.getInventory()
								.setItem(
										event.getPlayer().getInventory()
												.getHeldItemSlot(),
										getItemStack(false));
						event.getPlayer().updateInventory();
					}
				}
			} else {
				event.getPlayer()
						.sendMessage(
								ChatColor.GRAY
										+ "[Hub] "
										+ ChatColor.RED
										+ "You do not have permission to toggle player visibility");
			}
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.getPlayer().getInventory().setItem(slot, getItemStack(true));

		for (String hidingPlayer : hiding) {
			if (!event.getPlayer().hasPermission("hub.alwaysvisible")) {
				Bukkit.getPlayerExact(hidingPlayer).hidePlayer(
						event.getPlayer());
			}
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		for (Player pl : Bukkit.getOnlinePlayers()) {
			if (!event.getPlayer().canSee(pl)) {
				event.getPlayer().showPlayer(pl);
			}
		}

		synchronized (cooldown) {
			if (cooldown.containsKey(event.getPlayer().getName())) {
				cooldown.remove(event.getPlayer().getName());
			}
		}

		synchronized (hiding) {
			if (hiding.contains(event.getPlayer().getName())) {
				hiding.remove(event.getPlayer().getName());
			}
		}
	}

	public ItemStack getItemStack(boolean visible) {
		ItemStack stack = null;
		if (visible) {
			stack = new ItemStack(Material.EYE_OF_ENDER);
			ItemMeta meta = stack.getItemMeta();
			meta.setDisplayName(ChatColor.YELLOW + "Player Visibility"
					+ ChatColor.GRAY + " - " + ChatColor.GREEN + "Visible");
			stack.setItemMeta(meta);
		} else {
			stack = new ItemStack(Material.ENDER_PEARL);
			ItemMeta meta = stack.getItemMeta();
			meta.setDisplayName(ChatColor.YELLOW + "Player Visibility"
					+ ChatColor.GRAY + " - " + ChatColor.RED + "Hidden");
			stack.setItemMeta(meta);
		}
		return stack;
	}

	private boolean handleCooldown(Player player) {
		synchronized (cooldown) {
			if (cooldown.containsKey(player.getName())) {
				final long left = ((cooldown.get(player.getName()) / 1000) + 10)
						- (System.currentTimeMillis() / 1000);
				if (left > 0) {
					player.sendMessage(ChatColor.GRAY + "[Hub] "
							+ ChatColor.RED
							+ "You cannot toggle player visibility for " + left
							+ " more seconds!");
					return true;
				} else {
					cooldown.remove(player.getName());
					cooldown.put(player.getName(), System.currentTimeMillis());
					return false;
				}
			} else {
				cooldown.put(player.getName(), System.currentTimeMillis());
				return false;
			}
		}
	}

}
