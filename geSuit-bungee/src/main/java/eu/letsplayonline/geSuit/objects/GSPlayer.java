package eu.letsplayonline.geSuit.objects;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import eu.letsplayonline.geSuit.Utilities;

public class GSPlayer {

	private String playername;
	private UUID uuid;
	private boolean acceptingTeleports;
	private String server = null;
	private String ip;
	private Timestamp lastOnline;

	private HashMap<String, ArrayList<Home>> homes = new HashMap<>();
	private Location deathBackLocation;
	private Location teleportBackLocation;
	private boolean lastBack;
	private boolean firstConnect = true;

	public GSPlayer(String name, UUID uuid, boolean tps) {
		this(name, uuid, tps, null);
	}

	public GSPlayer(String name, UUID uuid, boolean tps, String ip) {
		this(name, uuid, tps, ip, null);
	}

	public GSPlayer(String name, UUID uuid, boolean tps, String ip,
			Timestamp lastOnline) {
		ProxyServer
				.getInstance()
				.getLogger()
				.info("LOADED DATA: " + name + " " + uuid + " " + tps + " "
						+ ip + " " + lastOnline);
		if (uuid == null) {
			Exception e = new Exception("Just for stacktrace");
			ProxyServer
					.getInstance()
					.getLogger()
					.log(Level.SEVERE,
							"Player "
									+ name
									+ " did not provide an UUID, Stacktrace follows!",
							e);
		}
		this.playername = name;
		this.uuid = uuid;
		this.acceptingTeleports = tps;
		this.ip = ip;
		this.lastOnline = lastOnline;
	}

	public String getName() {
		return playername;
	}

	public ProxiedPlayer getProxiedPlayer() {
		ProxyServer.getInstance().getPlayers();
		return ProxyServer.getInstance().getPlayer(uuid);
	}

	public void sendMessage(String message) {
		for (String line : message.split("\n")) {
			getProxiedPlayer().sendMessage(
					new TextComponent(Utilities.colorize(line)));
		}
	}

	public boolean acceptingTeleports() {
		return this.acceptingTeleports;
	}

	public void setAcceptingTeleports(boolean tp) {
		this.acceptingTeleports = tp;
	}

	public boolean hasDeathBackLocation() {
		return deathBackLocation != null;
	}

	public Location getLastBackLocation() {
		if (lastBack) {
			return deathBackLocation;
		} else {
			return teleportBackLocation;
		}
	}

	public boolean hasTeleportBackLocation() {
		return teleportBackLocation != null;
	}

	public Location getDeathBackLocation() {
		return deathBackLocation;
	}

	public void setDeathBackLocation(Location loc) {
		deathBackLocation = loc;
		lastBack = true;
	}

	public Location getTeleportBackLocation() {
		return teleportBackLocation;
	}

	public void setTeleportBackLocation(Location loc) {
		teleportBackLocation = loc;
		lastBack = false;
	}

	public String getServer() {
		if (getProxiedPlayer() == null) {
			return server;
		}

		return getProxiedPlayer().getServer() != null ? getProxiedPlayer()
				.getServer().getInfo().getName() : null;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public HashMap<String, ArrayList<Home>> getHomes() {
		return homes;
	}

	public boolean firstConnect() {
		return firstConnect;
	}

	public void connected() {
		firstConnect = false;
	}

	public void connectTo(ServerInfo s) {
		getProxiedPlayer().connect(s);
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getIp() {
		return ip;
	}

	public Timestamp getLastOnline() {
		return lastOnline;
	}
}
