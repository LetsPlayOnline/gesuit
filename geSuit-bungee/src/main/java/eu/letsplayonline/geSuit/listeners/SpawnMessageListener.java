package eu.letsplayonline.geSuit.listeners;

import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.managers.PlayerManager;
import eu.letsplayonline.geSuit.managers.SpawnManager;
import eu.letsplayonline.geSuit.objects.Location;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.spawns.GetSpawns;
import eu.letsplayonline.gesuit.common.messages.spawns.SendToProxySpawn;
import eu.letsplayonline.gesuit.common.messages.spawns.SetSpawn;

public class SpawnMessageListener extends ReceiverBase {

	@Override
	public void onSendToProxySpawn(final SendToProxySpawn message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					SpawnManager.sendPlayerToProxySpawn(PlayerManager
							.getPlayer(message.getPlayerName()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onGetSpawns(final GetSpawns message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					SpawnManager.sendSpawns(message.getSender());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onSetSpawn(final SetSpawn message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					eu.letsplayonline.gesuit.common.messages.Location loc = message
							.getSpawnLocation();
					switch (message.getSpawnType()) {
					case NEW_PLAYER:
						SpawnManager.setNewPlayerSpawn(
								PlayerManager.getPlayer(message.getPlayerName()),
								new Location(message.getSender(), loc
										.getWorld(), loc.getX(), loc.getY(),
										loc.getZ(), (float) loc.getYaw(),
										(float) loc.getPitch()));
						break;
					case PROXY:
						SpawnManager.setProxySpawn(
								PlayerManager.getPlayer(message.getPlayerName()),
								new Location(message.getSender(), loc
										.getWorld(), loc.getX(), loc.getY(),
										loc.getZ(), (float) loc.getYaw(),
										(float) loc.getPitch()));
						break;
					case SERVER:
						SpawnManager.setServerSpawn(
								PlayerManager.getPlayer(message.getPlayerName()),
								new Location(message.getSender(), loc
										.getWorld(), loc.getX(), loc.getY(),
										loc.getZ(), (float) loc.getYaw(),
										(float) loc.getPitch()), message
										.isExists());
						break;
					case WORLD:
						SpawnManager.setWorldSpawn(
								PlayerManager.getPlayer(message.getPlayerName()),
								new Location(message.getSender(), loc
										.getWorld(), loc.getX(), loc.getY(),
										loc.getZ(), (float) loc.getYaw(),
										(float) loc.getPitch()), message
										.isExists());
						break;
					default:
						break;

					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}
}
