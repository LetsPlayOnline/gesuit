package eu.letsplayonline.geSuit.listeners;

import java.net.InetSocketAddress;
import java.util.logging.Level;

import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.discovery.DiscoveryHello;

/**
 * Listener for the discovery hello messages.
 * 
 * @author jfrank
 *
 */
public class DiscoveryListener extends ReceiverBase {
	@Override
	public void onDiscoveryHello(DiscoveryHello message) {
		geSuit.instance.getLogger().log(Level.INFO,
				"Got a discovery hello message " + message);
		if (geSuit.proxy.getServers().containsKey(message.getSender())) {
			geSuit.proxy.getServers().remove(message.getSender());
		}
		geSuit.proxy.getServers()
				.put(message.getSender(),
						geSuit.proxy.constructServerInfo(message.getSender(),
								new InetSocketAddress(message.getHost(),
										message.getPort()),
								"Auto-added via discovery", false));
	}
}
