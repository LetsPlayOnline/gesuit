package eu.letsplayonline.geSuit.listeners;

import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.managers.PlayerManager;
import eu.letsplayonline.geSuit.managers.TeleportManager;
import eu.letsplayonline.geSuit.objects.GSPlayer;
import eu.letsplayonline.geSuit.objects.Location;
import eu.letsplayonline.geSuit.pluginmessages.TeleportToLocation;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersDeathBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.PlayersTeleportBackLocation;
import eu.letsplayonline.gesuit.common.messages.teleports.SendPlayerBack;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAHereRequest;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAReply;
import eu.letsplayonline.gesuit.common.messages.teleports.TPAll;
import eu.letsplayonline.gesuit.common.messages.teleports.TeleportToPlayer;
import eu.letsplayonline.gesuit.common.messages.teleports.ToggleTeleports;

public class TeleportsMessageListener extends ReceiverBase {

	@Override
	public void onTPAReply(final TPAReply message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					if (message.isAccept()) {
						TeleportManager.acceptTeleportRequest(PlayerManager
								.getPlayer(message.getPlayerName()));
					} else {
						TeleportManager.denyTeleportRequest(PlayerManager
								.getPlayer(message.getPlayerName()));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onTeleportToLocation(
			final eu.letsplayonline.gesuit.common.messages.teleports.TeleportToLocation message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					GSPlayer player = PlayerManager.getPlayer(message
							.getPlayerName());
					String server = message.getTargetServer();
					eu.letsplayonline.gesuit.common.messages.Location l = message
							.getTarget();
					TeleportToLocation.execute(
							player,
							new Location(
									(server != null && !server.equals("")) ? server
											: message.getSender(),
									l.getWorld(), l.getX(), l.getY(), l.getZ()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onPlayersTeleportBackLocation(
			final PlayersTeleportBackLocation message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					eu.letsplayonline.gesuit.common.messages.Location l = message
							.getTarget();
					TeleportManager.setPlayersTeleportBackLocation(
							PlayerManager.getPlayer(message.getPlayerName()),
							new Location(message.getSender(), l.getWorld(), l
									.getX(), l.getY(), l.getZ()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onPlayersDeathBackLocation(
			final PlayersDeathBackLocation message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					eu.letsplayonline.gesuit.common.messages.Location l = message
							.getTarget();
					TeleportManager.setPlayersDeathBackLocation(
							PlayerManager.getPlayer(message.getPlayerName()),
							new Location(message.getSender(), l.getWorld(), l
									.getX(), l.getY(), l.getZ()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onTeleportToPlayer(final TeleportToPlayer message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					TeleportManager.teleportPlayerToPlayer(
							message.getCommandSender(),
							message.getPlayerToTeleport(),
							message.getPlayerToTeleportTo(),
							message.isSilent(), message.isBypass());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onTPAHereRequest(final TPAHereRequest message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					if (message.isMeToHim()) {
						TeleportManager.requestToTeleportToPlayer(
								message.getPlayerName(),
								message.getTargetName());
					} else {
						TeleportManager.requestPlayerTeleportToYou(
								message.getPlayerName(),
								message.getTargetName());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onTPAll(final TPAll message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					TeleportManager.tpAll(message.getCommandSender(),
							message.getPlayerTarget(), message.getAffectedServer());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onSendPlayerBack(final SendPlayerBack message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					TeleportManager.sendPlayerToLastBack(
							PlayerManager.getPlayer(message.getPlayerName()),
							message.isDeath(), message.isTeleport());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onToggleTeleports(final ToggleTeleports message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					TeleportManager.togglePlayersTeleports(PlayerManager
							.getPlayer(message.getPlayerName()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}
}
