package eu.letsplayonline.geSuit.listeners;

import java.sql.SQLException;

import net.md_5.bungee.api.config.ServerInfo;
import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.managers.PlayerManager;
import eu.letsplayonline.geSuit.managers.PortalManager;
import eu.letsplayonline.geSuit.objects.GSPlayer;
import eu.letsplayonline.geSuit.objects.Location;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.portals.DeletePortal;
import eu.letsplayonline.gesuit.common.messages.portals.ListPortals;
import eu.letsplayonline.gesuit.common.messages.portals.RequestPortals;
import eu.letsplayonline.gesuit.common.messages.portals.SetPortal;
import eu.letsplayonline.gesuit.common.messages.portals.TeleportPlayerToPortal;

public class PortalsMessageListener extends ReceiverBase {

	@Override
	public void onTeleportPlayerToPortal(final TeleportPlayerToPortal message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					PortalManager.teleportPlayer(
							PlayerManager.getPlayer(message.getPlayerName()),
							message.getPortalType().toString(),
							message.getDestinationName());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onListPortals(final ListPortals message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					PortalManager.listPortals(PlayerManager.getPlayer(message
							.getPlayerName()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onDeletePortal(final DeletePortal message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					PortalManager.deletePortal(
							PlayerManager.getPlayer(message.getPlayerName()),
							message.getPortalName());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onSetPortal(final SetPortal message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				GSPlayer sender = PlayerManager.getPlayer(message
						.getPlayerName());
				eu.letsplayonline.gesuit.common.messages.Portal portal = message
						.getNewPortal();
				eu.letsplayonline.gesuit.common.messages.Location min = portal
						.getMinLocation();
				eu.letsplayonline.gesuit.common.messages.Location max = portal
						.getMaxLocation();

				try {
					PortalManager.setPortal(
							sender,
							portal.getPortalName(),
							portal.getPortalType().toString(),
							portal.getDestinationName(),
							portal.getFillType().toString(),
							new Location(getServer(message.getSender()), max
									.getWorld(), max.getX(), max.getY(), max
									.getZ()),
							new Location(getServer(message.getSender()), min
									.getWorld(), min.getX(), min.getY(), min
									.getZ()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onRequestPortals(final RequestPortals message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					PortalManager.getPortals(getServer(message.getSender()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	private ServerInfo getServer(String name) {
		return geSuit.proxy.getServerInfo(name);
	}
}
