package eu.letsplayonline.geSuit.listeners;

import java.sql.SQLException;

import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;
import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.managers.ConfigManager;
import eu.letsplayonline.geSuit.managers.PlayerManager;
import eu.letsplayonline.geSuit.managers.SpawnManager;

public class SpawnListener implements Listener {
	@EventHandler(priority = EventPriority.HIGHEST)
	public void sendPlayerToHub(final PostLoginEvent e) throws SQLException {
		if (ConfigManager.spawn.ForceAllPlayersToProxySpawn
				&& !SpawnManager.newPlayers.contains(e.getPlayer())) {
			if (SpawnManager.doesProxySpawnExist()) {
				SpawnManager.sendPlayerToProxySpawn(PlayerManager.getPlayer(e
						.getPlayer().getName()));
			} else {
				geSuit.instance
						.getLogger()
						.warning(
								"Wanted to use ForceAllPlayersToProxySpawn without a Proxy Spawn set");
			}
		}
	}
}
