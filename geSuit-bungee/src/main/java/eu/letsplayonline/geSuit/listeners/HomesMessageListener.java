package eu.letsplayonline.geSuit.listeners;

import eu.letsplayonline.geSuit.Utilities;
import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.managers.DatabaseManager;
import eu.letsplayonline.geSuit.managers.HomesManager;
import eu.letsplayonline.geSuit.managers.PlayerManager;
import eu.letsplayonline.geSuit.objects.GSPlayer;
import eu.letsplayonline.geSuit.objects.Location;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.homes.DeleteHome;
import eu.letsplayonline.gesuit.common.messages.homes.GetHomesList;
import eu.letsplayonline.gesuit.common.messages.homes.SendPlayerHome;
import eu.letsplayonline.gesuit.common.messages.homes.SetPlayerHome;

public class HomesMessageListener extends ReceiverBase {
	@Override
	public void onDeleteHome(final DeleteHome message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					HomesManager.deleteHome(message.getPlayerName(),
							message.getHomeName());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onSendPlayerHome(final SendPlayerHome message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					HomesManager.sendPlayerToHome(
							PlayerManager.getPlayer(message.getPlayerName()),
							message.getHomeName());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onSetPlayerHome(final SetPlayerHome message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					GSPlayer gsPlayer = PlayerManager.getPlayer(message
							.getPlayerName());

					if (gsPlayer == null) {
						gsPlayer = DatabaseManager.players.loadPlayer(message
								.getPlayerName());

						if (gsPlayer == null) {
							DatabaseManager.players.insertPlayer(
									new GSPlayer(message.getPlayerName(),
											Utilities.getUUID(message
													.getPlayerName()), true),
									"0.0.0.0");
							gsPlayer = DatabaseManager.players
									.loadPlayer(message.getPlayerName());
							gsPlayer.setServer(message.getSender());
						} else {
							gsPlayer.setServer(message.getSender());
						}
					}
					eu.letsplayonline.gesuit.common.messages.Location l = message
							.getHomeLocation();
					HomesManager.createNewHome(
							gsPlayer,
							message.getServerLimit(),
							message.getGlobalLimit(),
							message.getHomeName(),
							new Location(message.getSender(), l.getWorld(), l
									.getX(), l.getY(), l.getZ(), (float) l
									.getYaw(), (float) l.getPitch()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onGetHomesList(final GetHomesList message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					HomesManager.listPlayersHomes(PlayerManager
							.getPlayer(message.getPlayerName()));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}
}