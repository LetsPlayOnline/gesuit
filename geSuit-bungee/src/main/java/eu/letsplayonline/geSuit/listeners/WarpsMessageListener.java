package eu.letsplayonline.geSuit.listeners;

import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.managers.PlayerManager;
import eu.letsplayonline.geSuit.managers.WarpsManager;
import eu.letsplayonline.geSuit.objects.Location;
import eu.letsplayonline.gesuit.common.interfaces.ReceiverBase;
import eu.letsplayonline.gesuit.common.messages.warps.DeleteWarp;
import eu.letsplayonline.gesuit.common.messages.warps.GetWarpsList;
import eu.letsplayonline.gesuit.common.messages.warps.SetWarp;
import eu.letsplayonline.gesuit.common.messages.warps.WarpPlayer;

public class WarpsMessageListener extends ReceiverBase {

	@Override
	public void onWarpPlayer(final WarpPlayer message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					WarpsManager.sendPlayerToWarp(message.getPlayerName(),
							message.getPlayerName(), message.getWarpName(),
							message.isBypass());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onGetWarpsList(final GetWarpsList message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					WarpsManager.getWarpsList(message.getPlayerName(),
							message.isShowServerWarps(),
							message.isShowGlobalWarps(),
							message.isShowHiddenWarps(), message.isBypass());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onSetWarp(final SetWarp message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					eu.letsplayonline.gesuit.common.messages.Location l = message
							.getWarpLocation();
					WarpsManager.setWarp(
							PlayerManager.getPlayer(message.getPlayerName()),
							message.getWarpName(),
							new Location(message.getSender(), l.getWorld(), l
									.getX(), l.getY(), l.getZ(), (float) l
									.getYaw(), (float) l.getPitch()), message
									.isHidden(), message.isGlobal());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	public void onDeleteWarp(final DeleteWarp message) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {

			@Override
			public void run() {
				try {
					WarpsManager.deleteWarp(
							PlayerManager.getPlayer(message.getPlayerName()),
							message.getWarpName());
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}
}
