package eu.letsplayonline.geSuit.configs;

import eu.letsplayonline.geSuit.geSuit;
import net.cubespace.Yamler.Config.Config;

import java.io.File;

public class SpawnConfig extends Config {
    public Boolean SpawnNewPlayerAtNewspawn = false;
    public Boolean ForceAllPlayersToProxySpawn = false;
    public SpawnConfig() {
        CONFIG_FILE = new File(geSuit.instance.getDataFolder(), "spawns.yml");
    }
}
