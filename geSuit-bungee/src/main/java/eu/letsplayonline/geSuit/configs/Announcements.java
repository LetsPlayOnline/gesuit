package eu.letsplayonline.geSuit.configs;

import eu.letsplayonline.geSuit.configs.SubConfig.AnnouncementEntry;
import eu.letsplayonline.geSuit.geSuit;
import net.cubespace.Yamler.Config.Config;

import java.io.File;
import java.util.HashMap;

public class Announcements extends Config {
    public Boolean Enabled = true;
    public HashMap<String, AnnouncementEntry> Announcements = new HashMap<>();
    public Announcements() {
        CONFIG_FILE = new File(geSuit.instance.getDataFolder(), "announcements.yml");
    }
}


