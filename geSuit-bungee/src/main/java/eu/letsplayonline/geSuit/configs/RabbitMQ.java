package eu.letsplayonline.geSuit.configs;

import eu.letsplayonline.geSuit.geSuit;
import net.cubespace.Yamler.Config.Config;

import java.io.File;

/**
 * Created by Julian Fölsch on 08.11.15.
 *
 * @author Julian Fölsch
 */
public class RabbitMQ extends Config {
    public String host = "localhost";
    public String port = "5672";
    public String queue = "geSuit";

    public RabbitMQ(){
        CONFIG_FILE = new File(geSuit.instance.getDataFolder(), "rabbitmq.yml");
    }
}
