package eu.letsplayonline.geSuit.configs;

import eu.letsplayonline.geSuit.geSuit;
import net.cubespace.Yamler.Config.Config;

import java.io.File;

public class TeleportConfig extends Config {
    public Integer TeleportRequestExpireTime = 10;

    public TeleportConfig() {
        CONFIG_FILE = new File(geSuit.instance.getDataFolder(), "teleport.yml");
    }
}
