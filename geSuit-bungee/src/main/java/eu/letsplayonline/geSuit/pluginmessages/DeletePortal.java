package eu.letsplayonline.geSuit.pluginmessages;

import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.objects.Portal;
import eu.letsplayonline.gesuit.common.interfaces.Factory;

/**
 * @author geNAZt (fabian.fassbender42@googlemail.com)
 */
public class DeletePortal {
	public static String OUTGOING_CHANNEL = "geSuitPortals";

	public static void execute(final Portal p) {
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {
			@Override public void run() {
				geSuit.manager()
				.get()
				.send(Factory.newDeletePortalPhy(p.getServer().getName(),
						p.getName()));
			}
		});
		
	}
}
