package eu.letsplayonline.geSuit.pluginmessages;

import java.util.LinkedList;
import java.util.List;

import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.objects.Spawn;
import eu.letsplayonline.gesuit.common.interfaces.Factory;
import eu.letsplayonline.gesuit.common.messages.Location;

/**
 * @author geNAZt (fabian.fassbender42@googlemail.com)
 */
public class SendSpawn {
	public static String OUTGOING_CHANNEL = "geSuitSpawns";

	public static void execute(final List<Spawn> spl) {
		final List<eu.letsplayonline.gesuit.common.messages.Spawn> spawns = new LinkedList<>();
		for (Spawn sp : spl) {
			Location spawnLocation = new Location(sp.getLocation().getX(), sp
					.getLocation().getY(), sp.getLocation().getZ(), sp
					.getLocation().getWorld(), sp.getLocation().getYaw(), sp
					.getLocation().getPitch());
			eu.letsplayonline.gesuit.common.messages.Spawn spawn = new eu.letsplayonline.gesuit.common.messages.Spawn(
					sp.getName(), spawnLocation);
			spawns.add(spawn);
		}
		if (spl.size() > 0) {
			geSuit.proxy.getScheduler().runAsync(geSuit.instance,
					new Runnable() {
						@Override
						public void run() {
							geSuit.manager()
									.get()
									.send(Factory.newSendSpawns(spl.get(0)
											.getLocation().getServer()
											.getName(), spawns));
						}
					});
		}

	}
}
