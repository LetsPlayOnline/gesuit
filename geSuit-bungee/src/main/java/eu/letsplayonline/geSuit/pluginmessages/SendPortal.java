package eu.letsplayonline.geSuit.pluginmessages;

import java.util.LinkedList;
import java.util.List;

import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.objects.Portal;
import eu.letsplayonline.gesuit.common.interfaces.Factory;
import eu.letsplayonline.gesuit.common.messages.FillType;
import eu.letsplayonline.gesuit.common.messages.PortalType;

/**
 * @author geNAZt (fabian.fassbender42@googlemail.com)
 */
public class SendPortal {

	public static String OUTGOING_CHANNEL = "geSuitPortals";

	public static void execute(final List<Portal> pl) {
		try {
			final List<eu.letsplayonline.gesuit.common.messages.Portal> portals = new LinkedList<>();
			for (Portal p : pl) {
				eu.letsplayonline.gesuit.common.messages.Location min = new eu.letsplayonline.gesuit.common.messages.Location(
						p.getMin().getX(), p.getMin().getY(),
						p.getMin().getZ(), p.getMin().getWorld(), p.getMin()
								.getYaw(), p.getMin().getPitch());
				eu.letsplayonline.gesuit.common.messages.Location max = new eu.letsplayonline.gesuit.common.messages.Location(
						p.getMax().getX(), p.getMax().getY(),
						p.getMax().getZ(), p.getMax().getWorld(), p.getMax()
								.getYaw(), p.getMax().getPitch());
				eu.letsplayonline.gesuit.common.messages.Portal portal = new eu.letsplayonline.gesuit.common.messages.Portal(
						p.getName(), PortalType.valueOf(p.getType()
								.toUpperCase()), p.getDest(),
						FillType.valueOf(p.getFillType().toUpperCase()), min,
						max);
				portals.add(portal);
			}
			geSuit.proxy.getScheduler().runAsync(geSuit.instance,
					new Runnable() {
						@Override
						public void run() {
							geSuit.manager()
									.get()
									.send(Factory.newSendPortals(pl.get(0)
											.getServer().getName(), portals));
						}
					});

		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
