package eu.letsplayonline.geSuit.pluginmessages;

import net.md_5.bungee.api.ProxyServer;
import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.objects.GSPlayer;
import eu.letsplayonline.gesuit.common.interfaces.Factory;

/**
 * @author geNAZt (fabian.fassbender42@googlemail.com)
 */
public class TeleportToPlayer {
	public static String OUTGOING_CHANNEL = "geSuitTeleport";

	public static void execute(final GSPlayer player, final GSPlayer target) {
		if (!player.getServer().equals(target.getServer())) {
			player.getProxiedPlayer()
					.connect(
							ProxyServer.getInstance().getServerInfo(
									target.getServer()));
		}	geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {
			@Override public void run() {
				geSuit.manager()
				.get()
				.send(Factory.newTeleportToPlayer(ProxyServer.getInstance()
						.getServerInfo(target.getServer()).getName(),
						player.getName(), player.getName(), target.getName(),
						false, false));
			}
		});
		
	}
}
