package eu.letsplayonline.geSuit.pluginmessages;

import eu.letsplayonline.geSuit.geSuit;
import eu.letsplayonline.geSuit.objects.GSPlayer;
import eu.letsplayonline.geSuit.objects.Location;
import eu.letsplayonline.gesuit.common.interfaces.Factory;

/**
 * @author geNAZt (fabian.fassbender42@googlemail.com)
 */
public class TeleportToLocation {

	public static String OUTGOING_CHANNEL = "geSuitTeleport";

	public static void execute(final GSPlayer player, final Location location) {
		if (location.getServer() == null) {
			geSuit.instance
					.getLogger()
					.severe("Location has no Server, this should never happen. Please check");
			Exception exception = new Exception("");
			exception.printStackTrace();
		}

		geSuit.instance.getLogger().severe(
				"Is player.getServer() == null? "
						+ (player.getServer() == null));
		if (player.getServer() == null
				|| !player.getServer().equals(location.getServer().getName())) {
			player.getProxiedPlayer().connect(location.getServer());
		}

		final eu.letsplayonline.gesuit.common.messages.Location target = new eu.letsplayonline.gesuit.common.messages.Location(
				location.getX(), location.getY(), location.getZ(),
				location.getWorld(), location.getYaw(), location.getPitch());
		geSuit.proxy.getScheduler().runAsync(geSuit.instance, new Runnable() {
			@Override public void run() {
				geSuit.manager()
				.get()
				.send(Factory.newTeleportToLocation(location.getServer()
						.getName(), player.getName(), target));
			}
		});
		
	}
}
