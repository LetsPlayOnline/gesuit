package eu.letsplayonline.geSuit.managers;

import eu.letsplayonline.geSuit.configs.*;
import net.cubespace.Yamler.Config.InvalidConfigurationException;

/**
 * @author geNAZt (fabian.fassbender42@googlemail.com)
 */
public class ConfigManager {
    public static Announcements announcements = new Announcements();
    public static BansConfig bans = new BansConfig();
    public static MainConfig main = new MainConfig();
    public static SpawnConfig spawn = new SpawnConfig();
    public static TeleportConfig teleport = new TeleportConfig();
    public static Messages messages = new Messages();
    public static RabbitMQ rabbitMQ = new RabbitMQ();

    static {
        try {
            messages.init();
            announcements.init();
            bans.init();
            main.init();
            spawn.init();
            teleport.init();
            rabbitMQ.init();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
}
