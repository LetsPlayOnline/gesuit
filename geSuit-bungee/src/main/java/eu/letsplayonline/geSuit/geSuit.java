package eu.letsplayonline.geSuit;

import java.net.InetSocketAddress;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import eu.letsplayonline.geSuit.commands.BanCommand;
import eu.letsplayonline.geSuit.commands.MOTDCommand;
import eu.letsplayonline.geSuit.commands.ReloadCommand;
import eu.letsplayonline.geSuit.commands.SeenCommand;
import eu.letsplayonline.geSuit.commands.UnbanCommand;
import eu.letsplayonline.geSuit.database.ConnectionHandler;
import eu.letsplayonline.geSuit.database.convert.Converter;
import eu.letsplayonline.geSuit.listeners.BansListener;
import eu.letsplayonline.geSuit.listeners.BansMessageListener;
import eu.letsplayonline.geSuit.listeners.DiscoveryListener;
import eu.letsplayonline.geSuit.listeners.HomesMessageListener;
import eu.letsplayonline.geSuit.listeners.PlayerListener;
import eu.letsplayonline.geSuit.listeners.PortalsMessageListener;
import eu.letsplayonline.geSuit.listeners.SpawnListener;
import eu.letsplayonline.geSuit.listeners.SpawnMessageListener;
import eu.letsplayonline.geSuit.listeners.TeleportsMessageListener;
import eu.letsplayonline.geSuit.listeners.WarpsMessageListener;
import eu.letsplayonline.geSuit.managers.ConfigManager;
import eu.letsplayonline.geSuit.managers.DatabaseManager;
import eu.letsplayonline.geSuit.managers.LoggingManager;
import eu.letsplayonline.gesuit.common.IMessageManager;
import eu.letsplayonline.gesuit.common.MessageManager;
import eu.letsplayonline.gesuit.common.interfaces.Factory;

public class geSuit extends Plugin {

	public static geSuit instance;
	private static IMessageManager manager;
	public static ProxyServer proxy;

	public static IMessageManager manager() {
		return manager;
	}

	public void onEnable() {
		instance = this;
		LoggingManager.log(ChatColor.GREEN + "Starting geSuit");
		proxy = ProxyServer.getInstance();
		LoggingManager.log(ChatColor.GREEN + "Initialising Managers");

		ConnectionHandler connectionHandler = DatabaseManager.connectionPool
				.getConnection();
		connectionHandler.release();

		if (ConfigManager.main.ConvertFromBungeeSuite) {
			Converter converter = new Converter();
			converter.convert();
		}

		manager = new MessageManager();
		manager.setBrokerEndpoint(ConfigManager.rabbitMQ.host,
				ConfigManager.rabbitMQ.port);
		manager.receiveAll();
		manager.setServerName("bungee");
		manager.setQueue(ConfigManager.rabbitMQ.queue);
		registerListeners();
		getProxy().getScheduler().runAsync(this, new Runnable() {
			@Override
			public void run() {
				manager.initialize();
				// Ping all live servers.
				manager.get().send(Factory.newDiscoveryRequest());
			}
		});
		registerCommands();
	}

	private void registerCommands() {
		// A little hardcore. Prevent updating without a restart. But command
		// squatting = bad!
		if (ConfigManager.main.MOTD_Enabled) {
			proxy.getPluginManager().registerCommand(this, new MOTDCommand());
		}
		if (ConfigManager.main.Seen_Enabled) {
			proxy.getPluginManager().registerCommand(this, new SeenCommand());
		}
		proxy.getPluginManager().registerCommand(this, new UnbanCommand());
		proxy.getPluginManager().registerCommand(this, new BanCommand());
		proxy.getPluginManager().registerCommand(this, new ReloadCommand());
	}

	private void registerListeners() {
		getProxy().registerChannel("geSuitBans"); // Bans in

		manager.registerReceiver(new TeleportsMessageListener());
		manager.registerReceiver(new WarpsMessageListener());
		manager.registerReceiver(new HomesMessageListener());
		manager.registerReceiver(new PortalsMessageListener());
		manager.registerReceiver(new SpawnMessageListener());
		manager.registerReceiver(new DiscoveryListener());
		proxy.getPluginManager().registerListener(this, new PlayerListener());
		proxy.getPluginManager().registerListener(this,
				new BansMessageListener());
		proxy.getPluginManager().registerListener(this, new BansListener());
		proxy.getPluginManager().registerListener(this, new SpawnListener());
	}

	public void onDisable() {
		DatabaseManager.connectionPool.closeConnections();
	}
}
